// Generate intel

var intelConfig = {
	archer: 'archer',
	attack_ship: 'attack_ship',
	catapult: 'catapult',
	chariot: 'chariot',
	colonize_ship: 'colonize_ship',
	demolition_ship: 'demolition_ship',
	hoplite: 'hoplite',
	rider: 'rider',
	slinger: 'slinger',
	small_transporter: 'small_transporter',
	trireme: 'trireme',
	sword: 'sword',
	big_transporter: 'big_transporter',
	bireme: 'bireme',
	ferkyon: 'ferkyon',
	griffin: 'griffin',
	harpy: 'harpy',
	fury: 'fury',
	godsent: 'godsent'
};
var intelURL = 'http://grepointel.com/addintel.php';
var intelTimer;

function initIntel () {
	var mainReport = $('li[data-option-id=reports]');
	
	mainReport.unbind('click');
	mainReport.click(function () {
		intelTimer = window.setInterval(function () {
			console.log('Timer: intelTimer level 1');
			
			var dialog = $('div.ui-dialog');
			var title = dialog.find('span.ui-dialog-title');
			
			if ($('#ajax_loader').css('visibility') === 'hidden' && dialog.length > 0 && title.text() === 'Reports') {
				window.clearInterval(intelTimer);
				
				// Report was opened
				intelTimer = window.setInterval(function () {
					console.log('Timer: intelTimer level 2');
					
					dialog = $('div.ui-dialog');
					title = dialog.find('span.ui-dialog-title');
					var footer = dialog.find('div.game_list_footer');
					var buttonIntel = footer.find('a.button:contains(Intel)');
					var unitsOverview = dialog.find('div.report_units_overview');
					var leftSide = unitsOverview.children('div.report_units.report_side_attacker');
					var unitsType = leftSide.find('div.report_units_type');
					
					if (dialog.length > 0 && title.text() === 'Reports' && buttonIntel.length <= 0 && $('#report_report').length > 0) {
						defaultQueryString();
						
						// Attacker
						var attacker = dialog.find('#report_header').children('#report_sending_town').children('ul');
						var attackerTown = attacker.children('li.town_name').children('a').text();
						var attackerName = attacker.children('li.town_owner').children('a').text();
						intelURL = updateQueryStringParameter(intelURL, 'aname', attackerName);
						intelURL = updateQueryStringParameter(intelURL, 'atown', attackerTown);
						
						
						// Attack report
						if (leftSide.length > 0) {
							$.each(unitsType, function () {
								$.each(unitsType.children('div'), function () {
									var unit = $(this).children('div:first');
									var reportLost = $(this).children('span.report_losts');
									
									$.each(intelConfig, function (key, name) {
										if (unit.hasClass(name)) {
											intelURL = updateQueryStringParameter(intelURL, name, unit.children('span').text() + '(' + reportLost.text() + ')');
											
											return false;
										}
									});
								});
							});
						} else { // Support report
							unitsType = $('div#report_game_body').children('div:last').children('div');
							$.each(unitsType, function () {
								var unit = $(this);
								
								$.each(intelConfig, function (key, name) {
									if (unit.hasClass(name)) {
										intelURL = updateQueryStringParameter(intelURL, name, unit.children('span:first').text() + '(-0)');
										
										return false;
									}
								});
							});
						}
						
						var intelButton = '' +
							'<a class="button" href="' + intelURL + '" style="float: right; " target="_blank">' + 
								'<span class="left"><span class="right"><span class="middle">Intel</span></span><span style="clear: both; "></span></span>' +
							'</a>';
						
						footer.append(intelButton);
						
						
						var closeButton = getCloseButton(unitsType);
						// closeButton.unbind('click');
						closeButton.click(function () {
							window.clearInterval(intelTimer);
						});
					}
				}, 1000);
			}
		}, 1000);
	});
}

function defaultQueryString () {
	intelURL = 'http://grepointel.com/addintel.php';
	var date = new Date();
	var year = date.getUTCFullYear();
	var month = date.getUTCMonth();
	var day = date.getUTCDate();
	var hour = date.getUTCHours();
	var min = date.getUTCMinutes();
	var second = date.getUTCSeconds();
	
	if (month === 0) month = '01';
	
	intelURL = updateQueryStringParameter(intelURL, 'server', 'en80');
	intelURL = updateQueryStringParameter(intelURL, 't', 'z');
	intelURL = updateQueryStringParameter(intelURL, 'date', year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + second);
}

function updateQueryStringParameter(uri, key, value) {
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}
	else {
		return uri + separator + key + "=" + value;
	}
}


// ================ Common functions ================
function getCloseButton (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('a.ui-dialog-titlebar-close');
}