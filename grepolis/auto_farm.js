var tmpTowns = [];
var closeButton = '';
var skipTowns = [
	73194, // Shadow 04
	84194, // Shadow 16
	107877 // Shadow 11
];


// Timers
var farmTimer, tradeTimer;


// Flags
var tradeCompleted = false;
var tradeFailed = false;


// var farmLoop = window.setInterval(function () {
	// var serverTime = getServerTime();
	
	// if (compareTime(serverTime, '20:43:59')) {
		// clearTimer(farmLoop);
		
		// autoFarm('4h');
	// } else {
		// console.log('Next farm: 20:43:59');
	// }
// }, 10*60*1000); // 10 mins


var autoFarm = function (interval) {
	// Stop if loop again
	if (Game.townId in toObject(tmpTowns)) {
		tmpTowns = [];
		if (closeButton !== '')
			closeButton.trigger('click');
		closeButton = '';
		return false;
	}
	
	
	// Check interval and convert
	interval = convertStringToSeconds(interval.toString());
	
	
	// Open Farm window
	$('#overviews_link_hover_menu').find('a[name=farm_town_overview]').trigger('click');
	
	// Waiting for loading
	farmTimer = window.setInterval(function (){
		// Get elements
		var fLeftPanel = $('#fto_town_wrapper'); // Left panel
		var fRightPanel = $('#farm_town_wrapper'); // Right panel
		var fTownsList = fLeftPanel.find('ul#fto_town_list'); // The list of towns in left panel
		var fFarmOptions = fRightPanel.find('#farm_town_options').find('ul.game_list').find('div#time_options_wrapper'); // Farm options in right panel
		var fResourcesBar = fRightPanel.find('#farm_town_options').find('ul.game_list').find('div#fto_farm_claim_new_res'); // Resources bar in right panel
		var fFarmsList = fRightPanel.find('#farm_town_list').find('ul.fto_farm_list'); // Farms list in right panel
		closeButton = getCloseButton(fLeftPanel); // Close button
		var demandButton = fRightPanel.find('a#fto_claim_button');
		var fMenuTabs = getTabsMenu(fLeftPanel); // Farm dialog tab menu
		var castBar = $('div.casted_powers_area'); // Cast bar
		var loader = $('#ajax_loader'); // Loading icon
		var fWood = 0, fStone = 0, fIron = 0; // New resources
		var resFull = {}; // Full resources
		var currFirstFarmName = fFarmsList.children('li:first').children('a.gp_town_link').text();
		
	
		// Check if the town was selected
		var fTown = fTownsList.children('li:contains(' + Game.townName + ')');
		var fTownIsSelected = false;
		if (fTown.inlineStyle("border-right")) {
			fTownIsSelected = true;
		}
		
		
		// Wait until passed all conditions
		if (loader.css('visibility') === 'hidden' && fFarmOptions.length > 0 && fTownIsSelected) {
			clearTimer(farmTimer);
			
			console.log('=========== Start farming ===========');
			console.log('Town: ' + Game.townName);
			
			
			// Skip if current town is in skipTowns list
			if (skipTowns.indexOf(Game.townId) !== -1) {
				console.log('Status: Skipped because this town is in skipTowns list!');
				
				farmNext(interval, currFirstFarmName);

				return false;
			}
	
			
			// Check if farms are ready
			var chkFarm = fFarmsList.children('li:first').children('a.checkbox');
			if (!chkFarm.hasClass('checked')) {
				var nextDemand = fFarmsList.children('li:first').children('div.fp_property').children('span:last').text();
			
				console.log('Status: Skipped because farms have not been ready yet!');
				console.log('Next demand: ' + nextDemand);
				
				farmNext(interval, currFirstFarmName);
				
				return false;
			}
			
			
			// Select time option
			fFarmOptions.find('div.fto_time_checkbox.fto_' + interval).children('a:first').trigger('click');
			console.log('Time option: ' + convertSecondsToTime(interval));
			
			
			// Switch to Loot if Loot's true
			if (castBar.find('div').hasClass('forced_loyalty')) {
				fMenuTabs.find('a#fto_pillage').trigger('click');
				console.log('Mode: Loot');
			} else {
				fMenuTabs.find('a#fto_claim').trigger('click');
				console.log('Mode: Demand');
			}
			
			
			// Smart trade if resources are full
			$.each(fResourcesBar.children('span'), function () {
				// Get resources
				if ($(this).hasClass('wood')) {
					fWood = $(this).children('span').text();
				}
				if ($(this).hasClass('stone')) {
					fStone = $(this).children('span').text();
				}
				if ($(this).hasClass('iron')) {
					fIron = $(this).children('span').text();
				}
				
				// What is full?
				if ($(this).children('span').hasClass('town_storage_full')) {
					if ($(this).hasClass('wood')) {
						resFull.wood = fWood;
					}
					if ($(this).hasClass('stone')) {
						resFull.stone = fStone;
					}
					if ($(this).hasClass('iron')) {
						resFull.iron = fIron;
					}
				}
			});
			
			// There's full resource
			if (!$.isEmptyObject(resFull)) {
				// Skip if there are 2 resources are full
				if (Object.keys(resFull).length > 1) {
					console.log('Status: Skipped because resources are full!');
					console.log(resFull);
					
					farmNext(interval, currFirstFarmName);
					
					return false;
				}
				
				// Wood's full
				if ('wood' in resFull) {
					console.log('Status: Wood is full!');
					if (fIron <= fStone) { // Trade Wood to Iron
						console.log('Status: Trade wood to iron!');
						farmTrade(fFarmsList.children('li'), 'wood', 'iron', 'stone');
					} else { // Trade Wood to Stone
						console.log('Status: Trade wood to stone!');
						farmTrade(fFarmsList.children('li'), 'wood', 'stone', 'iron');
					}
				}
				
				// Stone's full
				if ('stone' in resFull) {
					console.log('Status: Stone is full!');
					if (fIron <= fWood) { // Trade Stone to Iron
						console.log('Status: Trade stone to iron!');
						farmTrade(fFarmsList.children('li'), 'stone', 'iron', 'wood');
					} else { // Trade Stone to Wood
						console.log('Status: Trade stone to wood!');
						farmTrade(fFarmsList.children('li'), 'stone', 'wood', 'iron');
					}
				}
				
				// Iron's full
				if ('iron' in resFull) {
					console.log('Status: Iron is full!');
					if (fWood <= fStone) { // Trade Iron to Wood
						console.log('Status: Trade iron to wood!');
						farmTrade(fFarmsList.children('li'), 'iron', 'wood', 'stone');
					} else { // Trade Iron to Stone
						console.log('Status: Trade iron to stone!');
						farmTrade(fFarmsList.children('li'), 'iron', 'stone', 'wood');
					}
				}
				
				
				// Waiting for trading
				farmTimer = window.setInterval(function (){
					if (tradeCompleted) {
						clearTimer(farmTimer);
						tradeCompleted = false;
						
						console.log('Status: Trade was completed!');
						
						// Reset resources
						fTown.trigger('click');
						
						// Waiting
						farmTimer = window.setInterval(function (){
							if (loader.css('visibility') === 'hidden') {
								clearTimer(farmTimer);
								
								console.log('Status: Resources was refreshed!');
								
								
								// Continue farming
								autoFarm(interval);
								
								return false;
							}
						}, 1000);
					}
					
					
					if (tradeFailed) {
						clearTimer(farmTimer);
						tradeFailed = false;
						
						console.log('Status: Skipped because resources are full and there is no Farm to trade!');
						
						farmNext(interval, currFirstFarmName);
						
						return false;
					}
				}, 1000);
			} else {
				// Demand or Loot
				demandButton.trigger('click');
				
				farmTimer = window.setInterval(function (){
					if (loader.css('visibility') === 'hidden') {
						clearTimer(farmTimer);
						
						
						console.log('Status: Farm was completed!');
						
						farmNext(interval, currFirstFarmName);
						
						return false;
					}
				}, 1000);
			}
		}
	}, 1000);
}

function nextTown () {
	window.HelperTown.switchToNextTown();
}

function farmNext (interval, prevFirstFarmName) {
	tmpTowns.push(Game.townId);
	var currTownId = Game.townId;
	
	nextTown();
	
	// Check waiting and loop again
	var loader = $('#ajax_loader');
	farmTimer = window.setInterval(function (){
		var fRightPanel = $('#farm_town_wrapper'); // Right panel
		var fFarmsList = fRightPanel.find('#farm_town_list').find('ul.fto_farm_list'); // Farms list in right panel
		var currFirstFarmName = fFarmsList.children('li:first').children('a.gp_town_link').text();
		if (loader.css('visibility') === 'hidden' && currTownId !== Game.townId && prevFirstFarmName !== currFirstFarmName) {
			clearTimer(farmTimer);
			
			autoFarm(interval);
			
			return false;
		}
	}, 1000);
}

function farmTrade (farmsList, resFrom, resTo, resInvert) {
	// Check trade capacity
	var town = getTown(Game.townId);
	var tradeCapacity = town.getAvailableTradeCapacity();
	var loader = $('#ajax_loader');
	
	if (tradeCapacity < 2000) {
		console.log('Trade capacity is less then 2000!');
		return false;
	}
	
	
	var foundFarm = false;
	$.each(farmsList, function () {
		var tradeProp = $(this).find('div.clearfix').children('div.fp_property:last');
		var ratio = tradeProp.children('.popup_ratio').text();
		
		if (tradeProp.children('span:first').hasClass('resource_' + resFrom + '_icon') &&
			tradeProp.children('span:last').hasClass('resource_' + resTo + '_icon')) {
			// Just trade if ratio >= 1
			var ratioTo = ratio.split(':')[1];
			
			if (ratioTo >= 1) {
				foundFarm = true;
				
				
				var farmLink = $(this).find('a.gp_town_link');
				farmLink.trigger('click');
			
			
				// Trade
				var ctxMenu = $('#context_menu');
				if (ctxMenu.length > 0) {
					ctxMenu.children('div#trading').trigger('mousedown');
					
					// Waiting
					tradeTimer = window.setInterval(function (){
						var tradeButton = $('div.farm_standard_bg').find('a:contains(Trade)');
						var tradeInput = $('div.farm_standard_bg').find('input[name=trade_input]');
						var tradeCloseButton = getCloseButton(tradeButton);
						
						if (loader.css('visibility') === 'hidden' && tradeButton.length > 0) {
							clearTimer(tradeTimer);
							
							
							tradeInput.val(2000);
							tradeInput.trigger('change');
							
							
							// Perform Trade
							tradeButton.trigger('click');
							
							
							// Waiting
							tradeTimer = window.setInterval(function (){
								if (loader.css('visibility') === 'hidden') {
									clearTimer(tradeTimer);
									
									tradeCompleted = true;
									
									// Close Trade window
									tradeCloseButton.trigger('click');
								}
							}, 1000);
						}
					}, 1000);
				}
			}
		}
		
		
		if (foundFarm) {
			return false;
		}
	});
	
	
	// Didn't find any Farm to trade
	if (!foundFarm) {
		if (resInvert === '') {
			console.log('Status: There is no Farm to trade!');
			tradeFailed = true;
			return false;
		}
	
	
		console.log('Status: There is no Farm to trade from ' + resFrom + ' to ' + resTo);
		console.log('Status: Inverting trade from ' + resFrom + ' to ' + resInvert);

		farmTrade(farmsList, resFrom, resInvert, '');
		
		return false;
	}
}

function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    rv[arr[i]] = arr[i];
  return rv;
}

(function ($) {
    $.fn.inlineStyle = function (prop) {
         var styles = this.attr("style"),
             value;
         styles && styles.split(";").forEach(function (e) {
             var style = e.split(":");
             if ($.trim(style[0]) === prop) {
                 value = style[1];           
             }                    
         });   
         return value;
    };
}(jQuery));

function convertTimeString (time) {
	var h = time.split(':')[0];
	var m = time.split(':')[1];
	var s = time.split(':')[2];
	
	if (typeof h === 'undefined') {
		h = '0';
	}
	if (typeof m === 'undefined') {
		m = '59';
	}
	if (typeof s === 'undefined') {
		s = '59';
	}
	
	if (h < 10 && h.length < 2) {
		h = '0' + h;
	}
	if (m < 10 && m.length < 2) {
		m = '0' + m;
	}
	if (s < 10 && s.length < 2) {
		s = '0' + s;
	}
	
	return h + ':' + m + ':' + s;
}

function compareTime (a, b) { // If a > b return true
	var d1 = new Date("2013/09/05 " + a).getTime()/1000;
	var d2= new Date("2013/09/05 " + b).getTime()/1000;
	
	if (d1 >= d2) {
		return true;
	}
	
	return false;
}

function convertSecondsToTime (s) {
	var hours = parseInt( s / 3600 ) % 24;
	var minutes = parseInt( s / 60 ) % 60;
	var seconds = s % 60;
	
	return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
}
function convertStringToSeconds (text) {
	if (text.indexOf('h') !== -1) {
		return 60*60*(text.replace('h', ''));
	}
	
	if (text.indexOf('m') !== -1) {
		return 60*(text.replace('m', ''));
	}
	
	return text;
}

function getServerTime () {
	var now = new Date();
	
	return now.getUTCHours() + ':' + now.getUTCMinutes() + ':' + now.getUTCSeconds();
}

function clearTimer (timer) {
	window.clearInterval(timer);
	timer = null;
}
function getTown (townId) {
	return window.ITowns.towns[townId];
}

function clearAllTimers () {
	for (var i = 1; i < 99999; i++)
        window.clearInterval(i);
}

function getCloseButton (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('a.ui-dialog-titlebar-close');
}
function getTabsMenu (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('div.menu_wrapper').children('ul.menu_inner');
}

