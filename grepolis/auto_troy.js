var eventTimer;
var stages = {
	29: 'stage_29'
};

var troyCheckerTimer = window.setInterval(function() {
	autoTroy();
}, 1000 * 60 * 60); //1h

function autoTroy() {
	// Open Troy Game
	var event = $('#happening_large_icon');
	if (event.length > 0) {
		event.trigger('click');

		// Wait
		eventTimer = window.setInterval(function() {
			console.log('Timer: eventTimer (' + eventTimer + ') lv 1');

			var troops = $('div.hercules2014_map.frame_my_army div.mercenaries_box div.mercenary');
			var skirmisher = 0;
			var ferocious_warrior = 0;
			var gastraphetes_archer = 0;
			var war_horse = 0;
			var myrmidon = 0;

			if (troops.length > 0) {
				clearTimer(eventTimer);

				var closeButton = getCloseButton(troops);

				// Get all troops
				skirmisher = parseInt(troops.eq(0).children('div.ct_top').children('div.ct_right').children('div.healthy').text());
				ferocious_warrior = parseInt(troops.eq(1).children('div.ct_top').children('div.ct_right').children('div.healthy').text());
				gastraphetes_archer = parseInt(troops.eq(2).children('div.ct_top').children('div.ct_right').children('div.healthy').text());
				war_horse = parseInt(troops.eq(3).children('div.ct_top').children('div.ct_right').children('div.healthy').text());
				myrmidon = parseInt(troops.eq(4).children('div.ct_top').children('div.ct_right').children('div.healthy').text());

				if (skirmisher > 0 || ferocious_warrior > 0 || gastraphetes_archer > 0 || war_horse > 0 || myrmidon > 0) {
					eventAttack();
				}

				closeButton.trigger('click');
			}
		}, 1000);
	}
}

function eventAttack() {
	var stagesHTML = $('div.hercules_map div.stages');
	$.each(stages, function(id, name) {
		var stage = stagesHTML.children('div.stage.campaign_map.stage_' + id + '.badge_attack1[data-stage_id=' + id + ']');

		stage.children('div.stage_hover').trigger('click');

		var subWindow = $('div.classic_sub_window');
		var troopButtons = subWindow.find('div.my_army.hercules_units_box').find('div.hercules2014_mercenaries_box.mercenaries_box').children('div.mercenary');

		$.each(troopButtons, function() {
			$(this).children('div.ct_top').children('div.ct_left').children('div:first').trigger('click');
		});

		subWindow.find('div.cbx_caption').trigger('click');

		var attackButton = subWindow.find('div.attack_button_container').children('div:first');
		attackButton.trigger('click');

		eventTimer = window.setInterval(function() {
			console.log('Timer: eventTimer (' + eventTimer + ') lv 2');

			subWindow = $('div.classic_sub_window');

			if (subWindow.hasClass('campaign_fight_result')) {
				clearTimer(eventTimer);

				subWindow.find('.btn_close').trigger('click');
			}
		}, 1000);
	});
}


function clearTimer(timer) {
	window.clearInterval(timer);
	timer = null;
}
function getCloseButton(child) {
	return child.parents('div.hercules2014').find('div.buttons_container').find('div.btn_wnd.close');
}