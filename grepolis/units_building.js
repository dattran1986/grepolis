var dummyData = {
	townId: 95605,
	unitName: 'attack_ship',
	amount: 1,
	cast: true,
	type: 'navy'
};


var buildUnitTimer;
function buildUnit (data) {
	var currTownId = Game.townId;
	
	if (currTownId !== data.townId) {
		// Go to Town
		switchTown(data.townId);
		
		buildUnitTimer = window.setInterval(function (){
			console.log('Timer: buildUnitTimer lv 1');
			
			if ($('#ajax_loader').css('visibility') === 'hidden' && currTownId !== Game.townId) {
				clearTimer(buildUnitTimer);
				
				buildUnit(data);
				return false;
			}
		}, 1000);
	} else {
		// addLog('=========== Build Unit ===========');
		// addLog('Town: ' + Game.townName);
		// addLog('Unit: ' + data.unitName);
		// addLog('Amount: ' + data.amount);
		
		// Open the Barracks or Harbour
		if (data.type === 'land') {
			$('#building_main_area_barracks').trigger('click');
		} else if (data.type === 'navy') {
			$('#building_main_area_docks').trigger('click');
		}
		
		buildUnitTimer = window.setInterval(function (){
			console.log('Timer: buildUnitTimer lv 2');
			
			var confirmButton = $('#unit_order_confirm');
			var castBar = $('div.casted_powers_area');
			
			if ($('#ajax_loader').css('visibility') === 'hidden' && confirmButton.length > 0) {
				clearTimer(buildUnitTimer);
				
				// Select unit
				var units = $('#unit_order').children('div#units');
				units.children('div#' + data.unitName).find('div#unit_order_tab_' + data.unitName).trigger('click');
				
				// Fill amount
				var input = $('#unit_order_info').find('input#unit_order_input');
				input.val(data.amount);
				input.trigger('change');
				
				// Cast
				if (data.cast) {
					// addLog('Cast before continue');
					
					var cast = $('#unit_order').children('div.grcrt_power').find('div.js-power-icon');
					var isCast = false;
					
					if (cast.hasClass('fertility_improvement')) { // Land units
						// Check if cast was called
						if (!castBar.find('div').hasClass('fertility_improvement')) {
							if (cast.hasClass('disabled')) {
								// addLog('Cast is disabled');
							} else {
								// cast.trigger('click');
								// addLog('Cast fertility_improvement');
								isCast = true;
							}
						}
					} else if (cast.hasClass('call_of_the_ocean')) { // Navy units
						// Check if cast was called
						if (!castBar.find('div').hasClass('call_of_the_ocean')) {
							if (cast.hasClass('disabled')) {
								// addLog('Cast is disabled');
							} else {
								// cast.trigger('click');
								// addLog('Cast call_of_the_ocean');
								isCast = true;
							}
						}
					}
					
					if (isCast) {
						buildUnitTimer = window.setInterval(function (){
							console.log('Timer: buildUnitTimer lv 4');
							
							castBar = $('div.casted_powers_area');
							
							if ($('#ajax_loader').css('visibility') === 'hidden' && castBar.find('div').hasClass('call_of_the_ocean')) {
								clearTimer(buildUnitTimer);
								
								// Confirm
								buildUnitConfirm();
							}
						}, 1000);
					} else {
						// Confirm
						// addLog('Cast was called!');
						buildUnitConfirm();
					}
				} else {
					// Confirm
					buildUnitConfirm();
				}
			}
		}, 1000);
	}
}

function buildUnitConfirm () {
	var confirmButton = $('#unit_order_confirm');
	var currentSlots = parseInt($('#current_building_order_queue_count').text());
	
	confirmButton.trigger('click');

	buildUnitTimer = window.setInterval(function (){
		console.log('Timer: buildUnitTimer lv 5');

		var isCompleted = false;
		if (currentSlots < parseInt($('#current_building_order_queue_count').text())) {
			isCompleted = true;
		}
		
		if ($('#ajax_loader').css('visibility') === 'hidden' && isCompleted) {
			clearTimer(buildUnitTimer);
			
			getCloseButton($('#unit_order_confirm')).trigger('click');
			
			// addLog('Unit was built successfully!');
		}
	}, 1000);
}



// ================ Common functions ================
function getCloseButton (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('a.ui-dialog-titlebar-close');
}
function switchTown (townId) {
	window.HelperTown.townSwitch(townId);
}
function clearTimer (timer) {
	window.clearInterval(timer);
	timer = null;
}

















// function buildUnit (json, socket, callback) {
	// json = jQuery.parseJSON(json);
	
	// // Switch town
	// switchTown(json.townId);
	
	// var url = 'http://en80.grepolis.com/game/building_barracks?town_id=' + json.townId + '&action=build&h=' + Game.csrfToken;
	// var param = JSON.stringify({
		// "unit_id": json.unit_id,
		// "amount": json.amount,
		// "town_id": json.townId,
		// "nlreq_id": (typeof Game.notification_last_requested_id === 'undefined') ? 0 : Game.notification_last_requested_id
	// });
	
	// $.ajax({
		// url:url,
		// type: "POST",
		// data: {json: param},
		// success:function(result){
			// result = jQuery.parseJSON(result);
			// console.log('Get data from Grepolis successfully!');
			// console.log(result);

			// // Return to server
			// console.log('Return to server with callback: ' + callback);
			// socket.emit(callback, result);
		// }
	// });
// }