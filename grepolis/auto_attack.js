var target = 'nml57';
var attackTimer, sendAttackTimer;

var heroesList = [
	'atalanta',
	'heracles',
	'urephon',
	'zuretha'
];

var ignoreTowns = {
};

var autoAttack = function () {
	// Open Attack planning
	$('#overviews_link_hover_menu').find('a[name=attack_planer]').trigger('click');
	
	// Waiting
	attackTimer = window.setInterval(function () {
		console.log('Timers: attackTimer');
		var planList = $('table.plan_list_table');
		
		if ($('#ajax_loader').css('visibility') === 'hidden' && planList.length > 0 || $('div.attacks_list_box div.game_border').length > 0) {
			clearTimer(attackTimer);
			
			// Open plan
			planList.find('td:contains(' + target + ') a').trigger('click');
			
			// Waiting
			attackTimer = window.setInterval(function () {
				console.log('Timers: attackTimer');
				var attackListBox = $('div.attacks_list_box div.game_border');
				
				if ($('#ajax_loader').css('visibility') === 'hidden' && attackListBox.length > 0) {
					clearTimer(attackTimer);
					
					
					// Auto attack
					var i = 0;
					attackTimer = window.setInterval(function () {
						console.log('Timers: attackTimer');
						
						var serverTime = getServerTime();
						var attacksList = attackListBox.find('ul.attacks_list');
						
						
						// All are done
						if (i === attacksList.children('li').length) {
							clearTimer(attackTimer);
							console.log('Stop auto attack!');
							return false;
						}
						
						
						$.each(attacksList.children('li'), function () {
							var attack = $(this);
							
							// Skip if completed
							if(attack.attr('data-completed') === '1') {
								return true;
							}

							var departureTime = attack.find('div.row3').text().replace("Departure: ", "");
							var day = departureTime.split(" at ")[0];
							var time = departureTime.split(" at ")[1];
							time = convertTimeString(time);
							
							if(day === 'today' && compareTime(serverTime, time)) {
								attack.css('background', 'none rgb(155, 168, 255)');
								attack.attr('data-completed', '1');
								
								var attackIcon = attack.find('.attack_icon');
								attackIcon.trigger('click');
								
								// Waiting
								sendAttackTimer = window.setInterval(function(){
									console.log('Timers: sendAttackTimer');
									
									var butAttack = $('#btn_plan_attack_town').prev();
									
									if ($('#ajax_loader').css('visibility') === 'hidden' && butAttack.length > 0) {
										clearInterval(sendAttackTimer);
										
										
										// Check if there is a hero
										var form = butAttack.parents('form.send_units_form');
										var landUnits = form.children('div.town_info_units').children('div.ground_units');
										var navyUnits = form.children('div.town_info_units').children('div.naval_units');
										var heroUnit = landUnits.children('div.heroes_pickup');
										
										// Check if there's a hero
										$.each(heroesList, function (index, hero) {
											if (heroUnit.children('div.icon_border').children('a').children('div').hasClass(hero)) {
												heroUnit.children('div.cbx_include_hero').trigger('click');
											
												return false;
											}
										});
										
										// Check if breakthrough
										if (!(Game.townName in ignoreTowns)) {
											var attackType = form.children('div.attack_type_wrapper');
											if (attackType.children('div.attack_type.attack_type_breach').length > 0) {
												attackType.children('div.attack_type.attack_type_breach').trigger('click');
											}
										}
										
										
										butAttack.trigger('click');
										
										// Waiting
										sendAttackTimer = window.setInterval(function () {
											console.log('Timers: sendAttackTimer');
											
											if ($('#ajax_loader').css('visibility') === 'hidden') {
												clearInterval(sendAttackTimer);
												
												getCloseButton(butAttack).trigger('click');
											}
										}, 1000);
									}
								}, 1000);
								
								i++;
								return false;
							}
						});
					}, 1000);
				}
			}, 1000);
		}
	}, 1000);
	
};



// ================ Common functions ================
function getCloseButton (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('a.ui-dialog-titlebar-close');
}
function getTabsMenu (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('div.menu_wrapper').children('ul.menu_inner');
}
function clearAllTimers () {
	for (var i = 1; i < 99999; i++)
        window.clearInterval(i);
}
function getTown (townId) {
	return window.ITowns.towns[townId];
}
function clearTimer (timer) {
	window.clearInterval(timer);
	timer = null;
}
function getServerTime () {
	var now = new Date();
	
	return now.getUTCHours() + ':' + now.getUTCMinutes() + ':' + now.getUTCSeconds();
}
function convertSecondsToTime (s) {
	var hours = parseInt( s / 3600 ) % 24;
	var minutes = parseInt( s / 60 ) % 60;
	var seconds = s % 60;
	
	return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
}
function convertStringToSeconds (text) {
	if (text.indexOf('h') !== -1) {
		return 60*60*(text.replace('h', ''));
	}
	
	if (text.indexOf('m') !== -1) {
		return 60*(text.replace('m', ''));
	}
	
	return text;
}
function convertTimeString (time) {
	var h = time.split(':')[0];
	var m = time.split(':')[1];
	var s = time.split(':')[2];
	
	if (typeof h === 'undefined') {
		h = '0';
	}
	if (typeof m === 'undefined') {
		m = '59';
	}
	if (typeof s === 'undefined') {
		s = '59';
	}
	
	if (h < 10 && h.length < 2) {
		h = '0' + h;
	}
	if (m < 10 && m.length < 2) {
		m = '0' + m;
	}
	if (s < 10 && s.length < 2) {
		s = '0' + s;
	}
	
	return h + ':' + m + ':' + s;
}
function compareTime (a, b) { // If a > b return true
	var d1 = new Date("2013/09/05 " + a).getTime()/1000;
	var d2= new Date("2013/09/05 " + b).getTime()/1000;
	
	if (d1 >= d2) {
		return true;
	}
	
	return false;
}
(function ($) {
    $.fn.inlineStyle = function (prop) {
         var styles = this.attr("style"),
             value;
         styles && styles.split(";").forEach(function (e) {
             var style = e.split(":");
             if ($.trim(style[0]) === prop) {
                 value = style[1];           
             }                    
         });   
         return value;
    };
}(jQuery));
function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    rv[arr[i]] = arr[i];
  return rv;
}
function nextTown () {
	window.HelperTown.switchToNextTown();
}
function switchTown (townId) {
	window.HelperTown.townSwitch(townId);
	
	//$('#building_main_area_barracks').trigger('click');
	//$('#unit_order').find('#slinger').find('#unit_order_tab_slinger').trigger('click');
	//$('#unit_order_count').find('input#unit_order_input').val('5');
	//$('#unit_order_count').find('a#unit_order_confirm').trigger('click');
}
function toArray (obj) {
	return $.map(obj, function(value, index) {
		return [value];
	});
}

function resetStyle () {
	var attackListBox = $('div.attacks_list_box');
	var gameBorder = attackListBox.find('div.game_border');
	var attacksList = gameBorder.find('ul.attacks_list');
	
	attacksList.children('li').attr('style', '')
	attacksList.children('li').removeAttr('data-completed');
}

// ============================================================================================