var tradeData = {
	from: 63830,
	to: 72400,
	resources: {
		wood: 100,
		stone: 100,
		iron: 100
	}
};

var tradeTimer;
function trade (tradeData) {
	var currTownId = Game.townId;
	var loader = $('#ajax_loader');
	
	if (currTownId !== tradeData.from) {
		switchTown(tradeData.from);
		
		tradeTimer = window.setInterval(function (){
			if (loader.css('visibility') === 'hidden' && currTownId !== Game.townId) {
				clearTimer(tradeTimer);
				
				trade(tradeData);
				return false;
			}
		}, 1000);
	} else {
		// Open Farm window
		$('#overviews_link_hover_menu').find('a[name=farm_town_overview]').trigger('click');
		
		// Waiting for loading
		tradeTimer = window.setInterval(function (){
			// Get elements
			var fLeftPanel = $('#fto_town_wrapper'); // Left panel
			var fTownsList = fLeftPanel.find('ul#fto_town_list'); // The list of towns in left panel
			closeButton = getCloseButton(fLeftPanel); // Close button
		
			
			// Wait until passed all conditions
			if (loader.css('visibility') === 'hidden' && fLeftPanel.length > 0) {
				clearTimer(tradeTimer);
				
				
				// Find toTown
				var toTown = fTownsList.children('li.town' + tradeData.to);
				
				
				// toTown click
				toTown.children('a.gp_town_link:first').trigger('click');
				var ctxMenu = $('#context_menu');
				
				if (ctxMenu.length > 0) {
					ctxMenu.children('div#trading').trigger('mousedown');
					
					// Waiting
					tradeTimer = window.setInterval(function (){
						var tradeButton = $('#trade_button');
						var woodInput = $('#trade_type_wood').children('div.body').find('input');
						var stoneInput = $('#trade_type_stone').children('div.body').find('input');
						var ironInput = $('#trade_type_iron').children('div.body').find('input');
						var tradeCloseButton = getCloseButton(tradeButton);
						var tradeMessage = $('#human_message');
						
						if (loader.css('visibility') === 'hidden' && tradeButton.length > 0 
							&& woodInput.length > 0 && stoneInput.length > 0 && ironInput.length > 0) {
							clearTimer(tradeTimer);
							
							
							// Fill in inputs
							woodInput.val(tradeData.resources.wood);
							woodInput.trigger('blur');
							stoneInput.val(tradeData.resources.stone);
							stoneInput.trigger('blur');
							ironInput.val(tradeData.resources.iron);
							ironInput.trigger('blur');
							
							
							// Send
							tradeButton.trigger('click');
							
							tradeTimer = window.setInterval(function (){
								if (tradeMessage.css('display') !== 'none') {
									clearTimer(tradeTimer);
									
									
									if (tradeMessage.hasClass('human_message_error')) {
										// Error
									}
									if (tradeMessage.hasClass('human_message_success')) {
										// Success
									}
									
									// Close
									tradeCloseButton.trigger('click');
									closeButton.trigger('click');
								}
							}, 1000);
						}
					}, 1000);
				}
			}
		}, 1000);
	}
}


// ================ Common functions ================
function getCloseButton (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('a.ui-dialog-titlebar-close');
}
function getTabsMenu (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('div.menu_wrapper').children('ul.menu_inner');
}
function clearAllTimers () {
	for (var i = 1; i < 99999; i++)
        window.clearInterval(i);
}
function getTown (townId) {
	return window.ITowns.towns[townId];
}
function clearTimer (timer) {
	window.clearInterval(timer);
	timer = null;
}
function getServerTime () {
	var now = new Date();
	
	return now.getUTCHours() + ':' + now.getUTCMinutes() + ':' + now.getUTCSeconds();
}
function convertSecondsToTime (s) {
	var hours = parseInt( s / 3600 ) % 24;
	var minutes = parseInt( s / 60 ) % 60;
	var seconds = s % 60;
	
	return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
}
function convertStringToSeconds (text) {
	if (text.indexOf('h') !== -1) {
		return 60*60*(text.replace('h', ''));
	}
	
	if (text.indexOf('m') !== -1) {
		return 60*(text.replace('m', ''));
	}
	
	return text;
}
function convertTimeString (time) {
	var h = time.split(':')[0];
	var m = time.split(':')[1];
	var s = time.split(':')[2];
	
	if (typeof h === 'undefined') {
		h = '0';
	}
	if (typeof m === 'undefined') {
		m = '59';
	}
	if (typeof s === 'undefined') {
		s = '59';
	}
	
	if (h < 10 && h.length < 2) {
		h = '0' + h;
	}
	if (m < 10 && m.length < 2) {
		m = '0' + m;
	}
	if (s < 10 && s.length < 2) {
		s = '0' + s;
	}
	
	return h + ':' + m + ':' + s;
}
function compareTime (a, b) { // If a > b return true
	var d1 = new Date("2013/09/05 " + a).getTime()/1000;
	var d2= new Date("2013/09/05 " + b).getTime()/1000;
	
	if (d1 >= d2) {
		return true;
	}
	
	return false;
}
(function ($) {
    $.fn.inlineStyle = function (prop) {
         var styles = this.attr("style"),
             value;
         styles && styles.split(";").forEach(function (e) {
             var style = e.split(":");
             if ($.trim(style[0]) === prop) {
                 value = style[1];           
             }                    
         });   
         return value;
    };
}(jQuery));
function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    rv[arr[i]] = arr[i];
  return rv;
}
function nextTown () {
	window.HelperTown.switchToNextTown();
}
function switchTown (townId) {
	window.HelperTown.townSwitch(townId);
	
	//$('#building_main_area_barracks').trigger('click');
	//$('#unit_order').find('#slinger').find('#unit_order_tab_slinger').trigger('click');
	//$('#unit_order_count').find('input#unit_order_input').val('5');
	//$('#unit_order_count').find('a#unit_order_confirm').trigger('click');
}
function toArray (obj) {
	return $.map(obj, function(value, index) {
		return [value];
	});
}

// ============================================================================================