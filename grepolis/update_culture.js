var updateCultureTimer;
function updateCulture() {
	$('#building_main_area_place').trigger('click');

	updateCultureTimer = window.setInterval(function() {
		console.log('Timer: updateCultureTimer lv 2');
		
		var placeParty = $('#place_party');

		if ($('#ajax_loader').css('visibility') === 'hidden' && placeParty.length > 0) {
			clearTimer(updateCultureTimer);
			
			var organizeButton = placeParty.find('a.button');
			organizeButton.trigger('click');
			
			updateCultureTimer = window.setInterval(function() {
				console.log('Timer: updateCultureTimer lv 3');
				
				placeParty = $('#place_party');
				organizeButton = placeParty.find('a.button');
				
				if ($('#ajax_loader').css('visibility') === 'hidden' && organizeButton.length <= 0) {
					clearTimer(updateCultureTimer);
					
					var closeButton = getCloseButton(placeParty);
					closeButton.trigger('click');
				}
			}, 1000);
		}
	}, 1000);
}




function getCloseButton (child) {
	return child.parents('div.ui-dialog').children('div.ui-dialog-titlebar').children('a.ui-dialog-titlebar-close');
}
function clearTimer (timer) {
	window.clearInterval(timer);
	timer = null;
}