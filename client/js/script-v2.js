var local = 'http://localhost:3000/';
var server = 'http://54.68.209.122/';
var socketUrl = local;
var townsInfo = {towns:{}};
var tradesInfo = localStorage.getItem("tradesInfo");
tradesInfo = jQuery.parseJSON(tradesInfo);
var cultures = localStorage.getItem('cultures');
cultures = jQuery.parseJSON(cultures);
var minCave = 100000;
var socket;

var unitTranslator = {
	archer: 'Archer',
	attack_ship: 'Light ship',
	catapult: 'Catapult',
	chariot: 'Chariot',
	colonize_ship: 'Colony ship',
	demolition_ship: 'Explosion ship',
	hoplite: 'Hoplite',
	rider: 'Horseman',
	slinger: 'Slinger',
	small_transporter: 'Small transporter',
	trireme: 'Trireme',
	sword: 'Sword',
	big_transporter: 'Big transporter',
	bireme: 'Bireme',
	ferkyon: 'Ferkyon',
	griffin: 'Griffin',
	harpy: 'Harpy',
	fury: 'Erinys',
	godsent: 'Divine envoy',
};

var buildingTranslator = {
	academy: 'Academy',
	barracks: 'Barracks',
	docks: 'Harbor',
	farm: 'Farm',
	hide: 'Cave',
	ironer: 'Silver mine',
	library: 'Library',
	lighthouse: 'Lighthouse',
	lumber: 'Timber camp',
	main: 'Senate',
	market: 'Marketplace',
	oracle: 'Oracle',
	statue: 'Statue',
	stoner: 'Quarry',
	storage: 'Warehouse',
	temple: 'Temple',
	theater: 'Theater',
	thermal: 'Thermal baths',
	tower: 'Tower',
	wall: 'City wall',
	trade_office: 'Trade office'
};


var defaultSkipTowns = [
	73645, // Shadow 06
	84194, // Shadow 16
	107877, // Shadow 11
	101551, // Shadow 12
	92114, // Shadow 25
	97209, // Shadow 24
	111123 // Shadow 13
];

var oluTowns = [
	'Shadow 02',
	'Shadow 04',
	'Shadow 06',
	'Shadow 10',
	'Shadow 12',
	'Shadow 14',
	'Shadow 18',
	'Shadow 20',
	'Shadow 22',
	'Shadow 24',
	'Shadow 32',
	'Shadow 34'
];
var dluTowns = [
	'Shadow 01',
	'Shadow 03',
	'Shadow 09',
	'Shadow 15',
	'Shadow 19',
	'Shadow 21',
	'Shadow 27',
	'Shadow 29',
	'Shadow 33'
];
var lsTowns = [
	'Shadow 08',
	'Shadow 16',
	'Shadow 26',
	'Shadow 28',
	'Shadow 30'
];
var briTowns = [
	'Shadow 05',
	'Shadow 07',
	'Shadow 11',
	'Shadow 13',
	'Shadow 17',
	'Shadow 23',
	'Shadow 25',
	'Shadow 31'
];


window.onload = function() {
	// Try to connect locally first
	socket = io.connect(local);
	initSocket();
	
	var checkSocketTimer = window.setInterval(function() {
		clearInterval(checkSocketTimer);
		
		if (!socket.connected) {
			console.log('Can not connect to local socket. Connect to server socket ...');
			socket.close();
			socket = io.connect(server);
			initSocket();
			socketUrl = server;
		}
	}, 3000);
	
	
	window.setInterval(function() {
		$('#butGetTowns').trigger('click');
	}, 60 * 60 * 1000); // 1 hour
};

function initSocket () {
	socket.on('ready', function(data) {
		console.log(data.msg);
		socket.emit('client', {message: 'Helper v.2 connected!'});
	});
	
	socket.on('towns', function(data) {
		if (data.type === 'town') {
			var prop = data.prop;
			var town = data.data;

			townsInfo.towns[town.id] = town;

			if (prop.isEnd) {
				hideLoading();

				setIsland();

				renderTowns(townsInfo);
				
				$('#butAutoFarm').css('display', '');
				$('input[name=chkSelFarmOption]').css('display', '');
				$('span[name=farmOption]').css('display', '');
				$("select[name=selFilter]").selectmenu({disabled: false});

				if ($("select[name=selTown].fromTown").val() != 0 && $("select[name=selTown].toTown").val() != 0) {
					tradeFillData($("select[name=selTown].fromTown").val(), $("select[name=selTown].toTown").val());
				}
			}
		}
	});

	// Logging
	socket.on('log', function(data) {
		$('#txtLogs').html($('#txtLogs').html() + data.message + '</br>');
	});

	// Build unit
	socket.on('unitBuilt', function(data) {
		console.log(data);

		hideLoading();

		$('#butGetTowns').trigger('click');
	});

	// Get server time
	socket.on('pushServerTime', function(data) {
		$('.server-time').html(data.time);
	});

	// Towns Trade Completed
	socket.on('tradeCompleted', function(data) {
		$.each(data.data, function(townId, tradeData) {
			if (tradesInfo === null) {
				tradesInfo = {};
			}
			if (typeof tradesInfo[townId] === 'undefined') {
				tradesInfo[townId] = {};
			}
			if (typeof tradesInfo[townId][tradeData.complete_at] === 'undefined') {
				tradesInfo[townId][tradeData.complete_at] = {};
			}

			tradesInfo[townId][tradeData.complete_at] = tradeData.resources;
		});

		localStorage.setItem('tradesInfo', JSON.stringify(tradesInfo));

		hideLoading();

		$('#butGetTowns').trigger('click');
	});

	// Culture updated
	socket.on('cultureUpdated', function(data) {
		console.log(data);
		
		if (cultures === null) {
			cultures = {};
		}
		if (typeof cultures[data.data.townId] === 'undefined') {
			cultures[data.data.townId] = {};
		}

		cultures[data.data.townId] = data.data.time;
		
		localStorage.setItem('cultures', JSON.stringify(cultures));

		hideLoading();
		
		$('#butGetTowns').trigger('click');
	});
}

$( document ).ready(function(){	
	initButtons();
	
	$('input[name=chkSelFarmOption]').each(function()
	{
		$(this).change(function()
		{
			$('input[name=chkSelFarmOption]').prop('checked', false);
			$(this).prop('checked', true);
		});
	});
});

function renderTowns (data) {
	var towns = data.towns;
	var townsList = $('#tab-overview ul.towns-list');

	// Sort data before loop
	towns = $.map(towns, function(value, index) {
		return [value];
	});
	towns.sort(SortByName);
	
	townsList.html('');
	$.each(towns, function(townId, town){
		renderTownBlock(town);
		
		// Add function for checkbox
		$('input[data-island=' + town.island_id + ']').each(function()
		{
			$(this).change(function()
			{
				if (!$(this).prop('checked')) {
					$(this).prop('checked', false);
				} else {
					$('input[data-island=' + town.island_id + ']').prop('checked', false);
					$(this).prop('checked', true);
				}
			});
		});
	});
	
	var dialogUnits = $('[aria-describedby=dialogUnits]');
	if (dialogUnits.length > 0 && dialogUnits.css('display') !== 'none') {
		var townId = parseInt(dialogUnits.find('button[name=butUnitBuild]').attr('data-id'));
		renderUnitsList(townsInfo.towns[townId]);
	}
	
	filter($("select[name=selFilter]").val());
}

function renderTownBlock (town) {
	// Get sample HTML
	var sampleTownBlockHtml = $('#sampleTownsList ul.towns-list li:first').clone();
	sampleTownBlockHtml.css('display', 'none');
	
	// Add data-id and name
	sampleTownBlockHtml.attr('data-id', town.id);
	sampleTownBlockHtml.attr('data-name', town.name);
	
	// Fill city name
	var cityName = sampleTownBlockHtml.find('h4.header span');
	cityName.html(town.name + ' (' + town.resources.population + ')');
	
	if (town.resources.population <= 0) {
		if (town.resources.wood >= 15000 && town.resources.stone >= 18000 && town.resources.iron >= 15000) {
			sampleTownBlockHtml.children('div:first').removeClass('bg-white');
			sampleTownBlockHtml.children('div:first').addClass('bg-green');
		} else {
			sampleTownBlockHtml.children('div:first').removeClass('bg-white');
			sampleTownBlockHtml.children('div:first').addClass('bg-blue');
		}
	} else if (town.resources.population >= 200) {
		sampleTownBlockHtml.children('div:first').removeClass('bg-white');
		sampleTownBlockHtml.children('div:first').addClass('bg-pink');
	}
	
	// Fill townId into checkbox
	// This is used for Auto farm function
	var chkCity = sampleTownBlockHtml.find('h4.header input');
	chkCity.attr('data-id', town.id);
	chkCity.attr('data-island', town.island_id);
	chkCity.prop('checked', true);
	if (defaultSkipTowns.indexOf(town.id) !== -1) {
		chkCity.prop('checked', false);
	}
	
	// Fill res-value
	var resBar = sampleTownBlockHtml.find('ul[name="resBar"]');
	var resBarTrade = sampleTownBlockHtml.find('ul[name="resBarTrade"]');
	var lastTrade = getLastTrade(town.id);
	$.each(resBar.children('li'), function(){
		// Wood
		if ($(this).children('span.title').text().indexOf("W") > -1) {
			$(this).children('span.res-value').text(town.resources.wood);
			if (town.resources.wood >= town.resources.storage) {
				$(this).children('span.res-value').addClass('red');
			}
			
			resBarTrade.children('li').children('span.title:contains(W)').next().text(town.resources.wood + lastTrade.wood);
		}
		// Stone
		if ($(this).children('span.title').text().indexOf("S") > -1) {
			$(this).children('span.res-value').text(town.resources.stone);
			if (town.resources.stone >= town.resources.storage) {
				$(this).children('span.res-value').addClass('red');
			}
			
			resBarTrade.children('li').children('span.title:contains(S)').next().text(town.resources.stone + lastTrade.stone);
		}
		// Iron
		if ($(this).children('span.title').text().indexOf("I") > -1) {
			$(this).children('span.res-value').text(town.resources.iron);
			if (town.resources.iron >= town.resources.storage) {
				$(this).children('span.res-value').addClass('red');
			}
			
			resBarTrade.children('li').children('span.title:contains(I)').next().text(town.resources.iron + lastTrade.iron);
		}
	});
	
	// Fill town-info
	var townInfo = sampleTownBlockHtml.find('ul[name="townInfo"]');
	$.each(townInfo.children('li'), function(){
		// Storage
		if ($(this).children('span.title').text().indexOf("Storage") > -1) {
			$(this).children('span.info-value').html(town.resources.storage);
		}
		// Population
		if ($(this).children('span.title').text().indexOf("Population") > -1) {
			$(this).children('span.info-value').html(town.resources.population);
		}
		// Favor
		if ($(this).children('span.title').text().indexOf("Favor") > -1) {
			var favorHtml = town.resources.favor;
			if(town.resources.favor >= 500) {
				favorHtml = '<span class="red">' + favorHtml + '</span>';
			}
			$(this).children('span.info-value').html(favorHtml);
		}
		// God
		if ($(this).children('span.title').text().indexOf("God") > -1) {
			$(this).children('span.info-value').html(town.god);
		}
		// Cave
		if ($(this).children('span.title').text().indexOf("Cave") > -1) {
			$(this).children('span.info-value').html(town.cave);
			if (parseInt(town.cave) <= minCave) {
				$(this).children('span.info-value').addClass('red');
			}
		}
		// Culture
		if ($(this).children('span.title').text().indexOf("Culture") > -1 && cultures !== null && 
				typeof cultures[town.id] !== 'undefined') {
			if ((new Date($('div.server-time').text() + ' GMT').getTime() / 1000) < cultures[town.id]) {
				var d = new Date(cultures[town.id] * 1000);
				$(this).children('span.info-value').html(convertTimeString(d.getUTCHours() + ':' + d.getUTCMinutes() + ':' + d.getUTCSeconds()) + ' ' + d.getUTCDate() + '/' + (d.getUTCMonth() + 1) + '/' + d.getUTCFullYear());
			}
		}
	});
	
	// Add data to button
	var buttons = sampleTownBlockHtml.find('button');
	buttons.attr('data-id', town.id);
	
	// Copy to towns list
	var townsList = $('#tab-overview ul.towns-list');
	townsList.append(sampleTownBlockHtml);
	
	
	// Button Units
	$('button[name=butUnits]').unbind('click');
	$('button[name=butUnits]').click(function () {
		var townId = $(this).attr('data-id');
		var town = townsInfo.towns[townId];
		
		// Replace title header
		var dialogUnits = $( "#dialogUnits" ).dialog( "instance" );
		if(typeof dialogUnits !== 'undefined') {
			$( "#dialogUnits" ).dialog( "destroy" );
		}
		$( "#dialogUnits" ).attr('title', town.name + ' (' + town.resources.population + ')');
		
		// Add townId to button Build
		$( "#dialogUnits" ).find('button[name=butUnitBuild]').attr('data-id', townId);
		$( "#dialogUnits" ).find('button[name=butUnitBuild]').unbind('click');
		$( "#dialogUnits" ).find('button[name=butUnitBuild]').click(function () {
			var townId = $(this).attr('data-id');
			var unitId = $(this).attr('data-unitId');
			var amount = $(this).prev('input[name=input]').val();
			var cast = $(this).parent().children('input[name=cast]');
			
			buildUnit(townId, unitId, amount, cast.prop('checked'));
		});
		
		// Fill data
		renderUnitsList(town);
		
		
		$( "#dialogUnits" ).dialog({
			autoOpen: false,
			show: {
				effect: "clip",
				duration: 500
			},
			hide: {
				effect: "clip",
				duration: 500
			},
			modal: true,
			width: 'auto'
		});
		$('#dialogUnits').dialog( "open" );
		dialogModelClick($('#dialogUnits'));
	});
	
	
	// Button Trade
	var butTrade = sampleTownBlockHtml.find('button[name=butTrade]');
	if (town.trade_capacity <= 0 || (town.resources.wood < 1000 && town.resources.stone < 1000 && town.resources.iron < 1000)) {
		butTrade.attr('disabled', 'disabled');
		butTrade.addClass('disabled');
		
		butTrade.parents('div.town-block').removeClass('bg-blue');
		butTrade.parents('div.town-block').removeClass('bg-white');
		butTrade.parents('div.town-block').addClass('bg-gray');
	} else {
		butTrade.unbind('click');
		butTrade.click(function () {
			renderTownsSelList($(this).attr('data-id'));
			
			if (typeof $("select[name=selTown]").selectmenu('instance') !== 'undefined')
			{
				$("select[name=selTown]").selectmenu("destroy");
			}
			$("select[name=selTown]").selectmenu({width: "auto"});
			
			$("select[name=selTown]").selectmenu({ change: function( event, ui ) { 
				tradeFillData($("select[name=selTown].fromTown").val(), $("select[name=selTown].toTown").val());
			}});
			
			// Fill data
			tradeFillData($("select[name=selTown].fromTown").val(), $("select[name=selTown].toTown").val());
			
			// Button butTradeSend
			$('button[name=butTradeSend]').unbind('click');
			$('button[name=butTradeSend]').click(function () {
				showLoading();
				
				var tradeData = {
					from: parseInt($("select[name=selTown].fromTown").val()),
					to: parseInt($("select[name=selTown].toTown").val()),
					resources: {
						wood: parseInt($('#dialogTrade div.left').find('ul[name=resInput] li').eq(0).children('input').val()),
						stone: parseInt($('#dialogTrade div.left').find('ul[name=resInput] li').eq(1).children('input').val()),
						iron: parseInt($('#dialogTrade div.left').find('ul[name=resInput] li').eq(2).children('input').val())
					}
				};
				
				// Send request to Server
				var url = socketUrl + "townsTrade";
				$.ajax({
					url:url,
					data: {
						type: 'twoway',
						callback: 'tradeCompleted',
						json: JSON.stringify({
							tradeData: tradeData
						})
					},
					type: "POST",
					success:function(result){
						console.log(result);
					}
				});
			});
			
			// Inputs
			$.each($('#dialogTrade div.left').find('ul[name=resInput] li').children('input'), function () {
				$(this).unbind('change');
				$(this).bind('change', function () {
					tradeInputChange();
				});
				
				$(this).keypress(function (e){
					if (e.keyCode == 13 && !$('#dialogTrade').find('button[name=butTradeSend]').hasClass('disabled')) {
						$('#dialogTrade').find('button[name=butTradeSend]').button().trigger('click');
					}
				});
				
				$(this).keyup(function (e){
					tradeInputChange();
				});
			});
			
			
			$( "#dialogTrade" ).dialog({
				autoOpen: false,
				show: {
					effect: "clip",
					duration: 500
				},
				hide: {
					effect: "clip",
					duration: 500
				},
				modal: true,
				width: 'auto'
			});
			$('#dialogTrade').dialog( "open" );
			dialogModelClick($('#dialogTrade'));
		});
	}
	
	// Button Culture
	var butCulture = sampleTownBlockHtml.find('button[name=butUpdCulture]');
	if (sampleTownBlockHtml.children('div.town-block').hasClass('bg-green') && sampleTownBlockHtml.find('ul[name=townInfo] li span.info-value:contains(No)').length > 0) {
		butCulture.unbind('click');
		butCulture.click(function() {
			showLoading();
			
			// Send request to Server
			var url = socketUrl + "culture";
			$.ajax({
				url: url,
				data: {
					type: 'twoway',
					callback: 'cultureUpdated',
					json: JSON.stringify({
						townId: town.id
					})
				},
				type: "POST",
				success: function(result) {
					console.log(result);
				}
			});
		});
	} else {
		butCulture.attr('disabled', 'disabled');
		butCulture.addClass('disabled');
	}
	
	// Button Building
	var butBuilding = sampleTownBlockHtml.find('button[name=butBuildings]');
	butBuilding.unbind('click');
	butBuilding.click(function() {
		renderBuildings(townsInfo.towns[$(this).attr('data-id')]);
		
		$( "#dialogBuildings" ).dialog({
			autoOpen: false,
			show: {
				effect: "clip",
				duration: 500
			},
			hide: {
				effect: "clip",
				duration: 500
			},
			modal: true,
			width: 'auto'
		});
		$('#dialogBuildings').dialog( "open" );
		dialogModelClick($('#dialogBuildings'));
	});
}

function renderBuildings (town) {
	var sampleBuildings = $('#sampleBuildings').clone();
	var table = sampleBuildings.children('table');
	var buildings = town.buildings;
	
	var special1 = {
		thermal: 'thermal'
	};
	var special2 = {
		tower: 'tower',
		statue: 'statue'
	};
	
	var maxLevel = {
		academy: 36,
		barracks: 30,
		docks: 30,
		farm: 45,
		hide: 10,
		ironer: 40,
		library: 1,
		lighthouse: 1,
		lumber: 40,
		main: 25,
		market: 30,
		oracle: 1,
		statue: 1,
		stoner: 40,
		storage: 35,
		temple: 30,
		theater: 1,
		thermal: 1,
		tower: 1,
		wall: 25,
		trade_office: 1
	};
	
	$.each(buildingTranslator, function(name, translation) {
		var gray = '';
		if (maxLevel[name] === buildings[name]) {
			gray = 'class="gray"';
		}

		if (typeof buildings[name] !== 'undefined' && buildings[name] > 0) {
			table.find('td[data-name=' + name + ']').html('<span ' + gray + '>' + translation + ' (' + buildings[name] + ')</span>');
		}

		if (typeof special1[name] !== 'undefined' && buildings[name] > 0) {
			table.find('td[data-name=special1]').html('<span ' + gray + '>' + translation + ' (' + buildings[name] + ')</span>');
		}

		if (typeof special2[name] !== 'undefined' && buildings[name] > 0) {
			table.find('td[data-name=special2]').html('<span ' + gray + '>' + translation + ' (' + buildings[name] + ')</span>');
		}
	});
	
	$('#dialogBuildings div').html('');
	$('#dialogBuildings div').append(sampleBuildings);
}

function renderUnitsList (town) {
	var sampleUnitsList = $('#sampleUnitsList').clone();
	var landList = sampleUnitsList.find('ul[name=land]');
	var navyList = sampleUnitsList.find('ul[name=navy]');
	var li = landList.find('li:first');
	var availableUnits = getAvailableUnits(town);
	// console.log(hardBuildUnits);
	

	// Land
	landList.html('');
	sampleUnitsList.find('h4[name=land]').html(sampleUnitsList.find('h4[name=land]').html() + ' (' + (7 - town.units_order.land_slots) + '/7)');
	$.each(town.units.land, function(name, value){
		// Don't list hero here
		if (name === town.hero || name in getHeroes()) {
			return true;
		}
		
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}
		
		var liLand = li.clone();
		
		// Get units order
		if (town.units_order.land[name] > 0) {
			value = value + '<span class="green"> (' + town.units_order.land[name] + ')</span>';
		}
		
		// Get units can built
		var totalCanBuilt = unitsCanBuilt(town, name, 'land');
		if (totalCanBuilt > 0) {
			value = value + '<span class="red"> (' + totalCanBuilt + ')</span>';
		}
		
		liLand.find('span.title').html(translate(name, unitTranslator) + ': ');
		liLand.find('span.unit-value').html(value);
		liLand.attr('data-build', totalCanBuilt);
		liLand.attr('data-unitId', name);
		
		landList.append(liLand);
	});
	$.each(town.units_order.land, function(name, value){
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}
		
		
		var liLand = li.clone();
		
		if (!(name in town.units.land)) {
			value = '<span class="green">' + value + '</span>';
			
			// Get units can built
			var totalCanBuilt = unitsCanBuilt(town, name, 'land');
			if (totalCanBuilt > 0) {
				value = value + '<span class="red"> (' + totalCanBuilt + ')</span>';
			}
			
			liLand.find('span.title').html(translate(name, unitTranslator) + ': ');
			liLand.find('span.unit-value').html(value);
			liLand.attr('data-build', totalCanBuilt);
			liLand.attr('data-unitId', name);
			
			landList.append(liLand);
		}
	});
	$.each(availableUnits.land, function(index, name){
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}
		
		
		var liLand = li.clone();
		var totalCanBuilt = unitsCanBuilt(town, name, 'land');
		
		if (!(name in town.units.land) && !(name in town.units_order.land)) {
			var value = '<span class="red">' + totalCanBuilt + '</span>';
			
			liLand.find('span.title').html(translate(name, unitTranslator) + ': ');
			liLand.find('span.unit-value').html(value);
			liLand.attr('data-build', totalCanBuilt);
			liLand.attr('data-unitId', name);
			
			landList.append(liLand);
		}
	});
	
	// Navy
	navyList.html('');
	sampleUnitsList.find('h4[name=navy]').html(sampleUnitsList.find('h4[name=navy]').html() + ' (' + (7 - town.units_order.navy_slots) + '/7)');
	$.each(town.units.navy, function(name, value){
		// Don't list hero here
		if (name === town.hero || name in getHeroes()) {
			return true;
		}
		
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}
		
		// Skip if militia
		if (name === 'militia') {
			return true;
		}
		
		
		var liNavy = li.clone();
		
		// Get units order
		if (town.units_order.navy[name] > 0) {
			value = value + '<span class="green"> (' + town.units_order.navy[name] + ')</span>';
		}
		
		// Get units can built
		var totalCanBuilt = unitsCanBuilt(town, name, 'navy');
		if (totalCanBuilt > 0) {
			value = value + '<span class="red"> (' + totalCanBuilt + ')</span>';
		}
		
		liNavy.find('span.title').html(translate(name, unitTranslator) + ': ');
		liNavy.find('span.unit-value').html(value);
		liNavy.attr('data-build', totalCanBuilt);
		liNavy.attr('data-unitId', name);
		
		navyList.append(liNavy);
	});
	$.each(town.units_order.navy, function(name, value){
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}
		
		
		var liNavy = li.clone();
		
		if (!(name in town.units.navy)) {
			value = '<span class="green">' + value + '</span>';
			
			// Get units can built
			var totalCanBuilt = unitsCanBuilt(town, name, 'navy');
			if (totalCanBuilt > 0) {
				value = value + '<span class="red"> (' + totalCanBuilt + ')</span>';
			}
			
			liNavy.find('span.title').html(translate(name, unitTranslator) + ': ');
			liNavy.find('span.unit-value').html(value);
			liNavy.attr('data-build', totalCanBuilt);
			liNavy.attr('data-unitId', name);
			
			navyList.append(liNavy);
		}
	});
	$.each(availableUnits.navy, function(index, name){
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}
		
		
		var liNavy = li.clone();
		var totalCanBuilt = unitsCanBuilt(town, name, 'navy');
		
		if (!(name in town.units.navy) && !(name in town.units_order.navy)) {
			var value = '<span class="red">' + totalCanBuilt + '</span>';
			
			liNavy.find('span.title').html(translate(name, unitTranslator) + ': ');
			liNavy.find('span.unit-value').html(value);
			liNavy.attr('data-build', totalCanBuilt);
			liNavy.attr('data-unitId', name);
			
			navyList.append(liNavy);
		}
	});
	
	// Transports need
	var transportsNeed = calTransportsNeed(town, sampleUnitsList);
	var bunks = '<span class="red">No</span>';
	if(town.researches.berth) {
		bunks = 'Yes';
	}
	if (sampleUnitsList.children('span.title').text().indexOf("Bunks") > -1) {
		sampleUnitsList.children('span[name=bunks]').html(bunks);
	}
	
	var transportsNeedHtml = sampleUnitsList.find('ul[name="transportsNeed"]');
	$.each(transportsNeedHtml.children('li'), function(){
		// Big transporter
		if ($(this).children('span.title').text().indexOf("Big") > -1) {
			$(this).children('span.unit-value').text('+' + transportsNeed.big_transporter);
		}
		// Small transporter
		if ($(this).children('span.title').text().indexOf("Small") > -1) {
			$(this).children('span.unit-value').text('+' + transportsNeed.small_transporter);
		}
		
		$(this).children('span.unit-value').addClass('red');
	});
	
	// Append
	$('#dialogUnits').children('div.units-list').html('');
	$('#dialogUnits').children('div.units-list').append(sampleUnitsList);
}

function isBuildUnit (townId, name) {
	var hardBuildUnits = getHardBuildUnits();
	
	if (typeof hardBuildUnits[townId] !== 'undefined') {
		var getHardBuildUnit = hardBuildUnits[townId];
		if (typeof getHardBuildUnit[name] === 'undefined') {
			return false;
		}
	}
	
	return true;
}

function unitClick (li) {
	$('#dialogUnits').find('input').val(li.attr('data-build'));
	$('#dialogUnits').find('button[name=butUnitBuild]').attr('data-unitId', li.attr('data-unitid'));
}

function buildUnit (townId, unitId, amount, cast) {
	if (typeof townId === 'undefined' || townId === '') {
		$('#dialogUnits').find('.error').html('Error: TownId is undefined!');
		$('#dialogUnits').find('.error').fadeIn(500);
		return false;
	}
	if (typeof unitId === 'undefined' || unitId === '') {
		$('#dialogUnits').find('.error').html('Please select a unit!');
		$('#dialogUnits').find('.error').fadeIn(500);
		return false;
	}
	if (typeof amount === 'undefined' || amount === '' || amount == 0) {
		$('#dialogUnits').find('.error').html('Please enter the amount!');
		$('#dialogUnits').find('.error').fadeIn(500);
		return false;
	}
	
	// Check if slot = 0
	var unitsCost = getUnitsCost();
	if (unitsCost[unitId].type === 'land') {
		// Land
		if (townsInfo.towns[townId].units_order.land_slots <= 0) {
			$('#dialogUnits').find('.error').html('Land units slots are full!');
			$('#dialogUnits').find('.error').fadeIn(500);
			return false;
		}
	} else {
		// Navy
		if (townsInfo.towns[townId].units_order.navy_slots <= 0) {
			$('#dialogUnits').find('.error').html('Navy units slots are full!');
			$('#dialogUnits').find('.error').fadeIn(500);
			return false;
		}
	}
	
	
	$('#dialogUnits').find('.error').fadeOut(500);
	
	// console.log(
		// {
			// townId: townId, 
			// unitName: unitId, 
			// amount: amount,
			// cast: cast,
			// type: unitsCost[unitId].type
		// }
	// );
	
	showLoading();
		
	// Send request to Server
	var url = socketUrl + "buildUnit";
	$.ajax({
		url:url,
		data: {
			type: 'twoway',
			callback: 'unitBuilt',
			json: JSON.stringify({
				townId: townId, 
				unitName: unitId, 
				amount: amount,
				cast: cast,
				type: unitsCost[unitId].type
			})
		},
		type: "POST",
		success:function(result){
			console.log(result);
		}
	});
}

function getAvailableUnits (town) {
	var availableUnits = {};
	availableUnits.land = ['sword'];
	availableUnits.navy = ['big_transporter'];
	
	var researches = town.researches;
	var unitsCost = getUnitsCost();
	
	$.each(unitsCost, function (name, attr) {
		if (researches[name]) {
			if (attr.type === 'land') {
				availableUnits.land.push(name);
			} else {
				availableUnits.navy.push(name);
			}
		}
	});
	
	if (typeof town.god !== 'undefined' && town.god !== '') {
		availableUnits.land.push('godsent');
	}
	
	return availableUnits;
}

function calTransportsNeed (town, sampleUnitsList) {
	var researches = town.researches;
	var additionalSlot = 0;
	var defaultBigTransport = 26;
	var defaultSmallTransport = 10;
	var currentCapability = 0;
	var needCapability = 0;
	var needTransports = {};
	
	needTransports.big_transporter = 0;
	needTransports.small_transporter = 0;
	
	if(researches.berth) {
		additionalSlot = 6;
	}
	
	// Get total transporter
	var bigTransporters = town.units.navy.big_transporter;
	var smallTransporters = town.units.navy.small_transporter;
	if (typeof bigTransporters === 'undefined') {
		bigTransporters = 0;
	}
	if (typeof smallTransporters === 'undefined') {
		smallTransporters = 0;
	}
	
	if (typeof town.units_order.navy.big_transporter !== 'undefined') {
		bigTransporters += town.units_order.navy.big_transporter;
	}
	if (typeof town.units_order.navy.small_transporter !== 'undefined') {
		smallTransporters += town.units_order.navy.small_transporter;
	}
	
	// Get current capability
	currentCapability = (parseInt(bigTransporters)*(defaultBigTransport + additionalSlot)) + 
						(parseInt(smallTransporters)*(defaultSmallTransport + additionalSlot));

	// Get total land units
	var landUnits = town.units.land;
	var landUnitsOrder = town.units_order.land;
	var unitsCost = getUnitsCost();
	var totalLandUnits = 0;
	$.each(landUnits, function (name, value) {
		if (unitsCost[name].characteristic === 'ground') {
			totalLandUnits += value * unitsCost[name].population;
		}
	});
	$.each(landUnitsOrder, function (name, value) {
		if (unitsCost[name].characteristic === 'ground') {
			totalLandUnits += value * unitsCost[name].population;
		}
	});
	
	//console.log(totalLandUnits);
	
	if (currentCapability < totalLandUnits) {
		needCapability = totalLandUnits - currentCapability;
		needTransports.big_transporter = Math.ceil(needCapability/(defaultBigTransport + additionalSlot), 0);
		needTransports.small_transporter = Math.ceil(needCapability/(defaultSmallTransport + additionalSlot), 0);
	} else {
		sampleUnitsList.find('span[name=freeSlots]').text(currentCapability - totalLandUnits);
	}

	return needTransports;
}

function initButtons () {
	// Init getData button event
	$('#butGetTowns').unbind('click');
	$('#butGetTowns').click(function() {
		showLoading();
		
		// Send request to Server
		var url = socketUrl + "getTowns";
		$.ajax({
			url:url,
			data: {
				type: 'twoway',
				callback: 'towns'
			},
			type: "POST",
			success:function(result){
				console.log(result);
			}
		});
	});
	
	// Init butIntel button event
	$('#butIntel').unbind('click');
	$('#butIntel').click(function() {
		// Send request to Server
		var url = socketUrl + "enableIntelLink";
		$.ajax({
			url:url,
			data: {
				type: 'oneway'
			},
			type: "POST",
			success:function(result){
				console.log(result);
			}
		});
	});
	
	// Init butLogs button event
	$('#butLogs').unbind('click');
	$('#butLogs').click(function() {
		// Button clear
		$('#butClearLogs').unbind('click');
		$('#butClearLogs').click(function() {
			$('#txtLogs').html('');
		});
		
		$('#dialogLogs').dialog( "open" );
		dialogModelClick($('#dialogLogs'));
	});
	
	// Init butAutoFarm button event
	$('#butAutoFarm').unbind('click');
	$('#butAutoFarm').click(function() {
		// Get skipTowns
		var skipTowns = [];
		$.each($('input[name=chkSelTown]'), function () {
			if (!$(this).prop('checked') && $(this).is(':visible')) {
				skipTowns.push(parseInt($(this).attr('data-id')));
			}
		});
		// console.log(skipTowns);
		
		
		// Get Farm options
		var farmOption = '5m';
		$.each($('input[name=chkSelFarmOption]'), function () {
			if ($(this).prop('checked') && $(this).is(':visible')) {
				farmOption = $(this).attr('value');
				return false;
			}
		});
		// console.log(farmOption);
		
		// Send request to Server
		var url = socketUrl + "autoFarm";
		$.ajax({
			url:url,
			data: {
				type: 'oneway',
				json: JSON.stringify({
					skipTowns: skipTowns,
					farmOption: farmOption
				})
			},
			type: "POST",
			success:function(result){
				console.log(result);
			}
		});
	});
	
	// Init butUnCheck button event
	$('#butUnCheck').unbind('click');
	$('#butUnCheck').click(function() {
		$('input[name=chkSelTown]').prop('checked', false);
	});
}

function filter (code) {
	var townsList = $('#tab-overview ul.towns-list').children('li');
	var i = 0;

	switch (code) {
		case 'white': // White towns
			townsList.fadeOut(500, function() {
				i++;
				
				if (i === Object.keys(townsInfo.towns).length) {
					townsList.children('div.bg-white').parent().fadeIn(500);
				}
			});
			
			break;
		case 'green': // Green towns
			townsList.fadeOut(500, function(){
				i++;
				
				if (i === Object.keys(townsInfo.towns).length) {
					townsList.children('div.bg-green').parent().fadeIn(500);
				}
			});
			
			break;
		case 'blue': // Blue towns
			townsList.fadeOut(500, function() {
				i++;
				
				if (i === Object.keys(townsInfo.towns).length) {
					townsList.children('div.bg-blue').parent().fadeIn(500);
				}
			});
			
			break;
		case 'red': // Red towns
			townsList.fadeOut(500, function() {
				i++;
				
				if (i === Object.keys(townsInfo.towns).length) {
					townsList.children('div.bg-pink').parent().fadeIn(500);
				}
			});
			
			break;
		case 'olu': // OLU nuke
			townsList.fadeOut(500, function() {
				i++;
				
				if (i === Object.keys(townsInfo.towns).length) {
					$.each(oluTowns, function(index, value){
						townsList.parent().children('li[data-name="'+value+'"]').fadeIn(500);
					});
				}
			});
			
			break;
		case 'dlu': // DLU nuke
			townsList.fadeOut(500, function() {
				i++;
				
				if (i === Object.keys(townsInfo.towns).length) {
					$.each(dluTowns, function(index, value){
						townsList.parent().children('li[data-name="'+value+'"]').fadeIn(500);
					});
				}
			});
			
			break;
		case 'ls': // LS nuke
			townsList.fadeOut(500, function() {
				i++;
				
				if (i === Object.keys(townsInfo.towns).length) {
					$.each(lsTowns, function(index, value){
						townsList.parent().children('li[data-name="'+value+'"]').fadeIn(500);
					});
				}
			});
			
			break;
		case 'bri': // Bri nuke
			townsList.fadeOut(500, function() {
				i++;
				
				if (i === Object.keys(townsInfo.towns).length) {
					$.each(briTowns, function(index, value){
						townsList.parent().children('li[data-name="'+value+'"]').fadeIn(500);
					});
				}
			});
			
			break;
		default: // All
			townsList.fadeIn(500);
			break;
	}
}

function dialogModelClick (dialog) {
	$('div.ui-widget-overlay.ui-front').unbind('click');
	$('div.ui-widget-overlay.ui-front').click(function () {
		dialog.dialog( "close" );
	});
}

function unitsCanBuilt (town, unitName, type) {
	var researches = town.researches;
	var unitCost = getUnitsCost()[unitName];

	if(type === 'land') {
		// Land units
		if(researches.conscription) {
			unitCost.wood = unitCost.wood - (unitCost.wood*10/100);
			unitCost.stone = unitCost.stone - (unitCost.stone*10/100);
			unitCost.iron = unitCost.iron - (unitCost.iron*10/100);
		}
	} else {
		// Navy units
		if(researches.mathematics) {
			unitCost.wood = unitCost.wood - (unitCost.wood*10/100);
			unitCost.stone = unitCost.stone - (unitCost.stone*10/100);
			unitCost.iron = unitCost.iron - (unitCost.iron*10/100);
		}
	}

	
	// Calculate total
	var total = 0;
	var condition = true;
	
	while (condition && total <= 1000) {
		if ((unitCost.wood * total) > town.resources.wood || 
			(unitCost.stone * total) > town.resources.stone ||
			(unitCost.iron * total) > town.resources.iron ||
			(unitCost.population * total) > town.resources.population ||
			(unitCost.favor * total) > town.resources.favor) {
			condition = false;
		} else {
			total++;
		}
	}

	return total - 1;
}

function renderTownsSelList (fromTownId) {
	$('select[name=selTown]').html('');
	var towns = townsInfo.towns;
	
	towns = $.map(towns, function(value, index) {
		return [value];
	});
	towns.sort(SortByName);
	
	$.each(towns, function (townId, town) {
		$('select[name=selTown]').append('<option value="' + town.id + '">' + town.name + '</option>');
	});
	
	
	$.each($('select[name=selTown].fromTown').children('option'), function () {
		if ($(this).val() == fromTownId) {
			$(this).attr('selected', 'selected');
		}
	});
}

function tradeFillData (fromTownId, toTownId) {
	var fromTown = townsInfo.towns[fromTownId];
	var toTown = townsInfo.towns[toTownId];
	
	var fromBlock = $('#dialogTrade div.left');
	var toBlock = $('#dialogTrade div.right');
	var defInput = getDefTrade(fromTown.trade_capacity);
	var lastTrade = getLastTrade(toTownId);
	
	// ResBar
	$.each(fromBlock.find('ul[name=resBar] li'), function () {
		// Wood
		if ($(this).children('span.title').text().indexOf("W") > -1) {
			$(this).children('span.res-value').text(fromTown.resources.wood);
			
			if (fromTown.resources.wood >= defInput)
				fromBlock.find('ul[name=resInput] li').children('span:contains(W)').next().val(defInput);
			else
				fromBlock.find('ul[name=resInput] li').children('span:contains(W)').next().val(fromTown.resources.wood);
		}
		// Stone
		if ($(this).children('span.title').text().indexOf("S") > -1) {
			$(this).children('span.res-value').text(fromTown.resources.stone);
			
			if (fromTown.resources.stone >= defInput)
				fromBlock.find('ul[name=resInput] li').children('span:contains(S)').next().val(defInput);
			else
				fromBlock.find('ul[name=resInput] li').children('span:contains(S)').next().val(fromTown.resources.stone);
		}
		// Iron
		if ($(this).children('span.title').text().indexOf("I") > -1) {
			$(this).children('span.res-value').text(fromTown.resources.iron);
			
			if (fromTown.resources.iron >= defInput)
				fromBlock.find('ul[name=resInput] li').children('span:contains(I)').next().val(defInput);
			else
				fromBlock.find('ul[name=resInput] li').children('span:contains(I)').next().val(fromTown.resources.iron);
		}
	});
	$.each(toBlock.find('ul[name=resBar] li'), function () {
		// Wood
		if ($(this).children('span.title').text().indexOf("W") > -1) {
			$(this).children('span.res-value').text(toTown.resources.wood);

			// Fill input
			var newWood = parseInt(toTown.resources.wood) + parseInt(lastTrade.wood) + parseInt(fromBlock.find('ul[name=resInput] li').eq(0).children('input').val());
			toBlock.find('ul[name=resInput] li').eq(0).children('input').val(newWood);
		}
		// Stone
		if ($(this).children('span.title').text().indexOf("S") > -1) {
			$(this).children('span.res-value').text(toTown.resources.stone);
			
			// Fill input
			var newStone = parseInt(toTown.resources.stone) + parseInt(lastTrade.stone) + parseInt(fromBlock.find('ul[name=resInput] li').eq(1).children('input').val());
			toBlock.find('ul[name=resInput] li').eq(1).children('input').val(newStone);
		}
		// Iron
		if ($(this).children('span.title').text().indexOf("I") > -1) {
			$(this).children('span.res-value').text(toTown.resources.iron);
			
			// Fill input
			var newIron = parseInt(toTown.resources.iron) + parseInt(lastTrade.iron) + parseInt(fromBlock.find('ul[name=resInput] li').eq(2).children('input').val());
			toBlock.find('ul[name=resInput] li').eq(2).children('input').val(newIron);
		}
	});
	
	// Capacity & Storage
	$.each(fromBlock.find('ul[name=townInfo] li'), function () {
		// Capacity
		if ($(this).children('span.title').text().indexOf("Capacity") > -1) {
			$(this).children('span.info-value').html(fromTown.trade_capacity);
			$(this).children('input[name=tradeCapacity]').val(fromTown.trade_capacity);
		}
	});
	$.each(toBlock.find('ul[name=townInfo] li'), function () {
		// Storage
		if ($(this).children('span.title').text().indexOf("Storage") > -1) {
			$(this).children('span.info-value').html(toTown.resources.storage);
		}
	});
	
	// Inputs
	fromBlock.find('ul[name=resInput] li').children('input').eq(0).select();
	
	tradeResClick(fromTown.trade_capacity);
	
	tradeInputChange();
}

function getLastTrade (townId) {
	var lastCapacity = {
		wood: 0,
		stone: 0,
		iron: 0
	};
	
	if (tradesInfo !== null && typeof tradesInfo[townId] !== 'undefined') {
		var tradeInfo = tradesInfo[townId];
		var serverTimeStamp = (new Date($('div.server-time').text() + ' GMT').getTime() / 1000);
		
		$.each(tradeInfo, function (completeAt, resources) {
			if (serverTimeStamp > completeAt) { // Completed
				delete tradesInfo[townId][completeAt];
				return true;
			}
			
			lastCapacity.wood += resources.wood;
			lastCapacity.stone += resources.stone;
			lastCapacity.iron += resources.iron;
		});
		
		localStorage.setItem('tradesInfo', JSON.stringify(tradesInfo));
	}
	
	return lastCapacity;
}

function tradeResClick (capacity) {
	var fromBlock = $('#dialogTrade div.left');
	
	$.each(fromBlock.find('ul[name=resInput] li').children('span'), function () {
		$(this).unbind('click');
		$(this).click(function () {
			if (parseInt($(this).next().val()) === 0) {
				if ($(this).text().indexOf("W") > -1) {
					if (parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(W)').next().text()) <= capacity)
						$(this).next().val(parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(W)').next().text()));
					else
						$(this).next().val(capacity);
				}
				
				if ($(this).text().indexOf("S") > -1) {
					if (parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(S)').next().text()) <= capacity)
						$(this).next().val(parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(S)').next().text()));
					else
						$(this).next().val(capacity);
				}
				
				if ($(this).text().indexOf("I") > -1) {
					if (parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(I)').next().text()) <= capacity)
						$(this).next().val(parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(I)').next().text()));
					else
						$(this).next().val(capacity);
				}
			} else {
				$(this).next().val('0');
			}
			
			tradeInputChange();
		});
	});
	
	fromBlock.find('ul[name=townInfo] li span:contains(Capacity)').unbind('click');
	fromBlock.find('ul[name=townInfo] li span:contains(Capacity)').click(function () {
		var defInput = getDefTrade(capacity);
		
		fromBlock.find('ul[name=resInput] li').children('input').val(defInput);
	});
}

function getDefTrade (capacity) {
	return Math.floor(capacity/3);
}

function tradeInputChange () {
	var fromBlock = $('#dialogTrade div.left');
	var toBlock = $('#dialogTrade div.right');
	var butTradeSend = $('#dialogTrade').find('button[name=butTradeSend]');
	var lastTrade = getLastTrade($("select[name=selTown].toTown").val());
	var storage = parseInt(toBlock.find('ul[name=townInfo] li:first span.info-value').text());
	
	$.each(fromBlock.find('ul[name=resInput] li').children('input'), function () {
		if (isNaN(parseInt($(this).val())))
			$(this).val('0');
	});
	
	var newWood = parseInt(fromBlock.find('ul[name=resInput] li').eq(0).children('input').val()) + 
					parseInt(toBlock.find('ul[name=resBar] li').eq(0).children('span.res-value').text()) +
					lastTrade.wood;
	var newStone = parseInt(fromBlock.find('ul[name=resInput] li').eq(1).children('input').val()) + 
					parseInt(toBlock.find('ul[name=resBar] li').eq(1).children('span.res-value').text()) +
					lastTrade.stone;
	var newIron = parseInt(fromBlock.find('ul[name=resInput] li').eq(2).children('input').val()) + 
					parseInt(toBlock.find('ul[name=resBar] li').eq(2).children('span.res-value').text()) +
					lastTrade.iron;
	
	toBlock.find('ul[name=resInput] li').eq(0).children('input').val(newWood);
	toBlock.find('ul[name=resInput] li').eq(1).children('input').val(newStone);
	toBlock.find('ul[name=resInput] li').eq(2).children('input').val(newIron);
	
	
	if (newWood >= storage) {
		toBlock.find('ul[name=resInput] li').eq(0).children('input').addClass('red');
	} else {
		toBlock.find('ul[name=resInput] li').eq(0).children('input').removeClass('red');
	}
	if (newStone >= storage) {
		toBlock.find('ul[name=resInput] li').eq(1).children('input').addClass('red');
	} else {
		toBlock.find('ul[name=resInput] li').eq(1).children('input').removeClass('red');
	}
	if (newIron >= storage) {
		toBlock.find('ul[name=resInput] li').eq(2).children('input').addClass('red');
	} else {
		toBlock.find('ul[name=resInput] li').eq(2).children('input').removeClass('red');
	}
	
	
	var tradeCapacity = fromBlock.find('ul[name=townInfo] li').eq(0).children('input[name=tradeCapacity]').val();
	var totalTrade = parseInt(fromBlock.find('ul[name=resInput] li').eq(0).children('input').val()) + 
					 parseInt(fromBlock.find('ul[name=resInput] li').eq(1).children('input').val()) + 
					 parseInt(fromBlock.find('ul[name=resInput] li').eq(2).children('input').val());
	var newCapacity = parseInt(tradeCapacity) - totalTrade;
	
	fromBlock.find('ul[name=townInfo] li').eq(0).children('span.info-value').html('');
	fromBlock.find('ul[name=townInfo] li').eq(0).children('span.info-value').html(
		tradeCapacity + '<span class="red"> - ' + totalTrade + ' = ' + newCapacity + '</span>'
	);
	
	if (newCapacity < 0 || parseInt(tradeCapacity) <= 0) {
		butTradeSend.addClass('disabled');
		butTradeSend.attr('disabled', 'disabled');
	} else {
		butTradeSend.removeClass('disabled');
		butTradeSend.removeClass('ui-button-disabled');
		butTradeSend.removeClass('ui-state-disabled');
		butTradeSend.removeAttr('disabled');
	}
}

function getUnitsCost () {
	return {
		sword: {wood: 95, stone: 0, iron: 85, population: 1, favor: 0, type: 'land', characteristic: 'ground'},
		archer: {wood: 120, stone: 0, iron: 75, population: 1, favor: 0, type: 'land', characteristic: 'ground'},
		hoplite: {wood: 0, stone: 75, iron: 150, population: 1, favor: 0, type: 'land', characteristic: 'ground'},
		slinger: {wood: 55, stone: 100, iron: 40, population: 1, favor: 0, type: 'land', characteristic: 'ground'},
		rider: {wood: 240, stone: 120, iron: 360, population: 3, favor: 0, type: 'land', characteristic: 'ground'},
		chariot: {wood: 200, stone: 440, iron: 320, population: 4, favor: 0, type: 'land', characteristic: 'ground'},
		catapult: {wood: 700, stone: 700, iron: 700, population: 15, favor: 0, type: 'land', characteristic: 'ground'},
		attack_ship: {wood: 1300, stone: 300, iron: 800, population: 10, favor: 0, type: 'navy', characteristic: 'sea'},
		bireme: {wood: 800, stone: 700, iron: 180, population: 8, favor: 0, type: 'navy', characteristic: 'sea'},
		trireme: {wood: 2000, stone: 1300, iron: 1300, population: 16, favor: 0, type: 'navy', characteristic: 'sea'},
		small_transporter: {wood: 800, stone: 0, iron: 400, population: 5, favor: 0, type: 'navy', characteristic: 'sea'},
		colonize_ship: {wood: 10000, stone: 10000, iron: 10000, population: 170, favor: 0, type: 'navy', characteristic: 'sea'},
		demolition_ship: {wood: 500, stone: 750, iron: 150, population: 8, favor: 0, type: 'navy', characteristic: 'sea'},
		big_transporter: {wood: 500, stone: 500, iron: 400, population: 7, favor: 0, type: 'navy', characteristic: 'sea'},
		centaur: {wood: 2300, stone: 900, iron: 400, population: 12, favor: 70, type: 'land', characteristic: 'ground'},
		pegasus: {wood: 4000, stone: 1300, iron: 700, population: 20, favor: 120, type: 'land', characteristic: 'sky'},
		griffin: {wood: 4100, stone: 2100, iron: 5200, population: 35, favor: 230, type: 'land', characteristic: 'sky'},
		harpy: {wood: 2000, stone: 500, iron: 1700, population: 14, favor: 85, type: 'land', characteristic: 'sky'},
		fury: {wood: 3300, stone: 6600, iron: 6600, population: 55, favor: 330, type: 'land', characteristic: 'ground'},
		godsent: {wood: 0, stone: 0, iron: 0, population: 3, favor: 12, type: 'land', characteristic: 'ground'},
	};
}

function getHardBuildUnits () {
	return {
		'77527': { // Shadow 03
			'sword': 830,
			'archer': 591,
			'hoplite': 328,
			'bireme': 80,
			'small_transporter': 109
		},
		'72400': { // Shadow 02
			'slinger': 1018,
			'rider': 213,
			'hoplite': 237,
			'attack_ship': 44,
			'small_transporter': 119
		},
		'107630': { // Shadow 18
			'slinger': 0,
			'rider': 0,
			'hoplite': 0,
			'catapult': 0,
			'griffin': 0,
			'godsent': 0,
			'attack_ship': 0,
			'small_transporter': 0,
			'colonize_ship': 0
		},
	};
}

function getHeroes () {
	return [
		'andromeda',
		'atalanta',
		'chiron',
		'democritus',
		'ferkyon',
		'helen',
		'heracles',
		'leonidas',
		'orpheus',
		'terylea',
		'urephon',
		'zuretha',
		'jason',
		'odysseus'
	];
}

function setIsland () { // Used for Auto Farm
	var islands = {
		189865: [63830], // Shadow 01
		192218: [72400], // Shadow 02
		192270: [77527], // Shadow 03
		192239: [73194, 73645], // Shadow 04, 06
		189834: [57859], // Shadow 05
		192364: [101188], // Shadow 07
		192314: [91228, 84194], // Shadow 08, 16
		189817: [107903], // Shadow 09
		192335: [95605], // Shadow 10
		197115: [107877, 107630], // Shadow 11, 18
		194750: [101551, 121201], // Shadow 12, 21
		190073: [111123, 111072], // Shadow 13, 32
		194798: [110947], // Shadow 14
		190074: [117929], // Shadow 15
		197103: [119779], // Shadow 17
		194746: [102441], // Shadow 19
		197190: [116013], // Shadow 20
		197061: [91839, 92114], // Shadow 22, 25
		192174: [61813], // Shadow 23
		197065: [97209, 112373], // Shadow 24, 30
		197082: [90365], // Shadow 26
		197034: [88220], // Shadow 27
		194754: [98306], // Shadow 28
		197018: [125007], // Shadow 29
	};
	
	$.each(townsInfo.towns, function (townId, town) {
		townId = parseInt(townId);
		$.each(islands, function (islandId, towns) {
			if (towns.indexOf(townId) !== -1) {
				townsInfo.towns[townId].island_id = parseInt(islandId);
				
				return false;
			}
		});
	});
}

function translate (key, translator) {
	if (key in translator) {
		return translator[key];
	}
	
	return key;
}

function showLoading() {
	$('.loading div').height($('body').height());

	$('.loading').css('display', '');
	
	$('.loading img').center();
}
function hideLoading() {
	$('.loading').css('display', 'none');
}

function SortByName(a, b){
	var aName = a.name.toLowerCase();
	var bName = b.name.toLowerCase(); 
	return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

function convertTimeString (time) {
	var h = time.split(':')[0];
	var m = time.split(':')[1];
	var s = time.split(':')[2];
	
	if (typeof h === 'undefined') {
		h = '0';
	}
	if (typeof m === 'undefined') {
		m = '59';
	}
	if (typeof s === 'undefined') {
		s = '59';
	}
	
	if (h < 10 && h.length < 2) {
		h = '0' + h;
	}
	if (m < 10 && m.length < 2) {
		m = '0' + m;
	}
	if (s < 10 && s.length < 2) {
		s = '0' + s;
	}
	
	return h + ':' + m + ':' + s;
}

$.fn.center = function () {   
	this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");    
	this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
	return this; 
};