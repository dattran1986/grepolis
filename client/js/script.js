var ownData = localStorage.getItem("ownData");
var unitsInTown = localStorage.getItem("unitsInTown");
var Game = localStorage.getItem("Game");

ownData = jQuery.parseJSON(ownData);
Game = jQuery.parseJSON(Game);
unitsInTown = jQuery.parseJSON(unitsInTown);

var unitTranslator = {
	archer: 'Archer',
	attack_ship: 'LS',
	catapult: 'Catapult',
	chariot: 'Chariot',
	colonize_ship: 'Colony',
	demolition_ship: 'Explosion',
	hoplite: 'Hoplite',
	rider: 'Horseman',
	slinger: 'Slinger',
	small_transporter: 'Fast transporter',
	trireme: 'Trireme',
	sword: 'Swordman',
	big_transporter: 'Transporter',
	bireme: 'Bireme',
};

var mythicalUnits = {
	'zeus': ['envoy', 'manticore', 'minotaur'],
	'poseidon': ['envoy', 'cyclop', 'hydra'],
	'hera': ['envoy', 'harpy', 'medusa'],
	'athena': ['envoy', 'centaur', 'pygasus']
};

var unitsCost = new Object();
resetUnitsCost();

window.onload = function() {
	var socket = io.connect('http://localhost:1357');
	
	//socket.emit('send', { message: "test" });
 
	socket.on('towndata', function (data) {
		console.log('Server returned data');
		console.log(data);

		ownData = data;
		
		// Set web storage
		clearStorage();
		localStorage.setItem("ownData", JSON.stringify(ownData));
		localStorage.setItem("last_update", getDate());
		
		generateTownHTML();
		
		localStorage.setItem("unitsInTown", JSON.stringify(unitsInTown));
		
		hideLoading();
	});
	
	socket.on('ready', function (data) {
		console.log(data.msg);
		socket.emit('client', {message: 'Helper connected!'});
	});
	
	socket.on('pullgame', function (data) {
		console.log('Pull Game successfully!');
		Game = data;
		//console.log(Game);
		localStorage.setItem("Game", JSON.stringify(Game));
	});
}

$( document ).ready(function(){
	if(unitsInTown === null) {
		unitsInTown = new Object();
	}
	
	if(ownData === null) {
		ownData = new Object();
		console.log("There is no cache!");
	} else {
		console.log("Get cache!");
		generateTownHTML();
	}
	
	// Init getData button event
	$('#getData').click(function() {
		showLoading();
		
		// Send request to Server
		var url = "http://localhost:1357/gettowndata";
		$.ajax({
			url:url,
			type: "POST",
			data: {},
			success:function(result){
				console.log(result);
			}
		});
	});
	
	$('#getMessages').click(function(){
		alert('ok');
	});
	
	$( "#convertTime" ).click(function() {
		$( "#dialogConverTime" ).dialog( "open" );
    });
	
	$( "#convert" ).click(function() {
		var hour = $('#inputHour');
		var utc = $('#inputUTC');
		
		var resultUTC = parseInt(hour.val()) + (0 - parseInt(utc.val()));
		if(resultUTC > 24) {
			resultUTC -= 24;
		}
		if(resultUTC < 0) {
			resultUTC += 24;
		}
		
		var resultUTC7 = parseInt(hour.val()) + (7 - parseInt(utc.val()));
		if(resultUTC7 > 24) {
			resultUTC7 -= 24;
		}
		if(resultUTC7 < 0) {
			resultUTC7 += 24;
		}
		
		$('#utc').text('UTC = ' + resultUTC.toString());
		$('#utc7').text('UTC+7 = ' + resultUTC7.toString());
    });
	
	$( "#selTownId" ).selectmenu({ change: function( event, ui ) { 
		var id = $(this).val();
		
		tinyMCE.get('msgContent').setContent(tinyMCE.get('msgContent').getContent({ format: 'text' }) + ' [town]' + id + '[/town]');

		$( "#selTownId" ).val('0').selectmenu('refresh');
	}});
});

function generateTownHTML () {
	$( "#accordion" ).remove();
	$( "#tabs-1" ).append('<div id="accordion"></div>');
	
	var townsInfo = ownData.towns_info;
	
	// Sort data before loop
	townsInfo = $.map(townsInfo, function(value, index) {
		return [value];
	});
	townsInfo.sort(SortByName);
	
	$('#selTown').html('');
	$('#selTownId').html('');
	$.each(townsInfo, function(index, town){
		// Generate town list
		generateTownList(town.name, town.id);
		
		// Check if full storage
		var wood = town.resources.wood;
		var stone = town.resources.stone;
		var iron = town.resources.iron;
		var storage = town.storage;
		var woodHTML = '<span class="res-col">W = ' + town.resources.wood + '</span>';
		var stoneHTML = '<span class="res-col">S = ' + town.resources.stone + '</span>';
		var ironHTML = '<span class="res-col">C = ' + town.resources.iron + '</span>';
		
		if(wood >= storage) {
			woodHTML = '<span class="res-col">W = <span style="color: red;">' + town.resources.wood + '</span></span>';
		}
		if(stone >= storage) {
			stoneHTML = '<span class="res-col">S = <span style="color: red;">' + town.resources.stone + '</span></span>';
		}
		if(iron >= storage) {
			ironHTML = '<span class="res-col">C = <span style="color: red;">' + town.resources.iron + '</span></span>';
		}
		
		// Check if espionage storage is low
		var espionage_storage = '<span>' + town.espionage_storage + '</span>';
		if(town.espionage_storage < 10000) {
			espionage_storage = '<span style="color: red;">' + town.espionage_storage + '</span>';
		}
		
		// Get building orders
		var buildingOrders = '';
		var count = 0;
		if(!isUndefined(town.building_orders)) {
			buildingOrders += '<div><ul class="building-order" style="display: none;">';
			$.each(town.building_orders, function(i, v){
				buildingOrders += '<li>' + v.name + '</li>';
				count++;
			});
			buildingOrders += '</ul></div>';
		}
		if(count <= 0) {
			buildingOrders = '<span style="color: red;">' + count + ' / 2</span>' + buildingOrders;
		} else {
			buildingOrders = '<span>' + count + ' / 2</span>' + buildingOrders;
		}
		
		
		// Get unit HTML
		var unitsHTML = generateUnitsHTML(town.id);
		
	
		var html = '' + 
			'<h3>' + town.name + ' (' + town.available_population + ')</h3>' + 
			'<div>' + 
				'<div>' + 
					'<span class="title">Resources: </span>' + 
					woodHTML + 
					stoneHTML + 
					ironHTML + 
					'<span class="res-col">Max = ' + town.storage + '</span>' + 
				'</div>' + 
				'<div><span class="title">Conquest: </span><span>' + town.conquest + '</span></div>' + 
				'<div><span class="title">Has conqueror: </span><span>' + town.has_conqueror + '</span></div>' + 
				'<div><span class="title">Espionage storage: </span>' + espionage_storage + '</div>' + 
				'<div class="buildingOrders"><span class="title">Building orders: </span>' + buildingOrders + '</div>' + 
				unitsHTML.html() +
				'<div><button name="buildUnit" data-townid="' + town.id + '">Build units</button></div>' + 
			'</div>'
		;
		
		$( "#accordion" ).append(html);
	});
	
	// Load notifications
	var notifications = ownData.notifications;
	var totalMsgs = 0;
	var totalReports = 0;
	var totalOthers = 0;
	$( "#messages" ).html('');
	$( "#reports" ).html('');
	$( "#others" ).html('');
	$.each(notifications, function(i, notification){
		if(notification.type === "newmessage") {
			totalMsgs++;
		} else if(notification.type === "newreport") {
			totalReports++;
		} else if(notification.type !== "backbone" && notification.type !== "systemmessage"){
			totalOthers++;
		}
		
		generateNoti(notification);
	});
	$( "#messages" ).parent().prev().text('Messages' + ' (' + totalMsgs + ')');
	$( "#reports" ).parent().prev().text('Reports' + ' (' + totalReports + ')');
	$( "#others" ).parent().prev().text('Others' + ' (' + totalOthers + ')');
	$('a[href="#tabs-2"]').text('Notifications' + ' (' + (totalMsgs + totalReports + totalOthers) + ')');
	
	
	$( "#accordion" ).accordion({heightStyle: "content"});
	
	$('#date').html(localStorage.getItem("last_update"));
	
	$('.buildingOrders').click(function(){
		$(this).find('div ul').slideToggle();
	});
	
	$('button[name=buildUnit]').click(function(){
		var townId = $(this).attr('data-townid');
		var availableUnitTypes = getAvailableUnitTypes(townId);
		var needTransports = calTransport(townId, availableUnitTypes.land.total, availableUnitTypes.navy);
		
		// Generate unit type list
		generateUnitTypesList(availableUnitTypes, needTransports, townId);
		
		var town = ownData.towns_info[townId];
		$('#availablePoppulation span[name=availablePop]').html(town.available_population);
	
		$('.error.select-unit-type').css('display', 'none');
		$( "#dialogBuildUnits" ).dialog( "open", {width: '500px'} );
	});
	
	autoCheckRed();
}

function generateUnitsHTML (townId) {
	var town = ownData.towns_info[townId];
	var units = town.units;
	var unitOrders = town.unit_orders;
	var unitHTML = $('div[name=townUnits]').clone();
	
	// Get unit orders
	var landCount = 0;
	var navyCount = 0;
	var unitsBuilding = new Object();
	if(!isUndefined(unitOrders)) {
		$.each(unitOrders, function(i, v){
			var li = '<li>' + v.name + ' = ' + v.count + '</li>';
			
			unitHTML.find('div[name=unitOrders] div ul:first').append(li);
			
			if(v.kind === 'ground') {
				landCount++;
			} else {
				navyCount++;
			}
			
			if(isUndefined(unitsBuilding[v.name])) {
				unitsBuilding[v.name] = 0;
			}
			unitsBuilding[v.name] += parseInt(v.count);
		});
	}
	
	unitHTML.find('[name=lblNavyOrders]').html(navyCount + ' / ' + Game.max_unit_orders);
	unitHTML.find('[name=lblLandOrders]').html(landCount + ' / ' + Game.max_unit_orders);
	
	// Get units
	/* Moved units to popup build units
	$.each(units, function(k, v){
		if(v !== 0) {
			var li = '<li>' + k + ' = ' + v + '</li>';;
			if(!isUndefined(unitsBuilding[k])) {
				li = '<li>' + k + ' = ' + v + ' + <span style="color: green;">' + unitsBuilding[k] + ' = ' + (v + unitsBuilding[k]) + '</span></li>';
			}
			
			unitHTML.find('div[name=units] div ul:first').append(li);
		}
	});
	*/
	
	return unitHTML;
}

function generateNoti (notification) {
	var type = notification.type;
	var subj = notification.subject;
	var paramId = notification.param_id;
	var paramStr = '';
	
	if(type === 'newaward') {
		subj = 'New award: ' + notification.param_str;
	} else {
		try
		{
			paramStr = jQuery.parseJSON(notification.param_str);
		}
		catch(e)
		{
			console.log('JSON parse failed: ' + notification.param_str);
		}
	}	
	
	if (type === "newmessage") {
		$( "#messages" ).append('<li><a href="#">' + subj + '</a> - <span>' + paramStr.player_name + '</span></li>');
	} else if (type === "newreport") {
		$( "#reports" ).append('<li><a href="#">' + subj + '</a></li>');
	} else if (type !== "backbone" && type !== "systemmessage") {
		if(type === 'phoenician_salesman_arrived') {
			subj = 'Phoenician salesman arrived @ ' + ownData.towns_info[paramId].name;
		}
		$( "#others" ).append('<li><span>' + subj + '</span></li>');
	}
}

function generateTownList (name, id) {
	$('#selTown').append('<option value="' + id + '">' + name + '</option>');
	$('#selTownId').append('<option value="' + id + '">' + name + '</option>');
}

function generateUnitTypesList (unitTypes, needTransports, townId) {
	$('[name=chkUnits] ul').html('');
	$('ul[name=transportNeed]').html('');
	
	// Land
	$.each(unitTypes.land, function(k, v){
		if(k === 'total') return true;
		
		var name = unitTranslator[k];
		if(v === true) v = 0;
		var value = name + ': ' + v;
		
		var unitCount = calMaxUnits(townId, 'land', k);
		if(unitCount > 0) {
			$('[name=chkUnits] ul[name=land]').append('<li><label><input data-type="land" class="chb" type="checkbox" name="chkUnit" value="' + k + '">' + value + '<span style="color: red;"> (' + unitCount + ')</span></label></li>');
		} else {
			$('[name=chkUnits] ul[name=land]').append('<li><label class="disabled"><input disabled="disabled" data-type="land" class="chb" type="checkbox" name="chkUnit" value="' + k + '">' + value + '</label></li>');
		}
	});
	
	// Navy
	$.each(unitTypes.navy, function(k, v){
		if(k === 'total') return true;
		
		var name = unitTranslator[k];
		if(v === true) v = 0;
		var value = name + ': ' + v;
		
		var unitCount = calMaxUnits(townId, 'navy', k);
		if(unitCount > 0) {
			$('[name=chkUnits] ul[name=navy]').append('<li><label><input data-type="navy" class="chb" type="checkbox" name="chkUnit" value="' + k + '">' + value + '<span style="color: red;"> (' + unitCount + ')</span></label></li>');
		} else {
			$('[name=chkUnits] ul[name=navy]').append('<li><label class="disabled"><input disabled="disabled" data-type="navy" class="chb" type="checkbox" name="chkUnit" value="' + k + '">' + value + '</label></li>');
		}
	});
	
	// Generate need transport
	$('ul[name=transportNeed]').append('<li><span>Big transporter: </span><span>' + needTransports.big_transporter + '<span style="color: green;"> * 7 = ' + (needTransports.big_transporter*7) + '</span></span>');
	$('ul[name=transportNeed]').append('<li><span>Small transporter: </span><span>' + needTransports.small_transporter + '<span style="color: green;"> * 5 = ' + (needTransports.small_transporter*5) + '</span></span></li>');
	
	
	// Check boxes event
	$(".chb").each(function() {
		$(this).change(function() {
			$(".chb").prop('checked',false);
			$(this).prop('checked',true);
			
			// Get max unit count
			var unitCount = calMaxUnits(townId, $(this).attr('data-type'), $(this).val());
			$('[name=txtUnitInput]').val(unitCount);
		});
	});
	
	// Button event
	$('[name=buildUnitsOK]').unbind('click');
	$('[name=buildUnitsOK]').click(function(){
		var chkBox = $('input[name="chkUnit"]:checked');
		if(isUndefined(chkBox.val())) {
			$('.error.select-unit-type').css('display', 'block');
			return false;
		} else {
			$('.error.select-unit-type').css('display', 'none');
		}
	});
}

function autoCheckRed () {
	var accordion = $('#accordion');
	var contents = accordion.children('div');
	
	$.each(contents, function(){
		var content = $(this);
		var red = content.find('span[style*="color: red"]');
		
		if(red.length > 0) {
			content.prev().css('color', 'red');
			return;
		}
	});
}

function getAvailableUnitTypes (townId) {
	var town = ownData.towns_info[townId];
	var units = town.units;
	var unitOrders = town.unit_orders;
	var unitKeys = new Object();
	var availableUnitTypes = new Object();
	unitKeys.land = ['archer', 'chariot', 'hoplite', 'rider', 'slinger', 'catapult', 'sword'];
	unitKeys.navy = ['attack_ship', 'bireme', 'trireme', 'small_transporter', 'colonize_ship', 'demolition_ship', 'big_transporter'];
	
	availableUnitTypes.land = new Object();
	availableUnitTypes.navy = new Object();
	
	// Get unit orders
	var unitsBuilding = new Object();
	if(!isUndefined(unitOrders)) {
		$.each(unitOrders, function(i, v){			
			if(isUndefined(unitsBuilding[v.name])) {
				unitsBuilding[v.name] = 0;
			}
			unitsBuilding[v.name] += parseInt(v.count);
		});
	}
	
	town.researches.sword = true;
	town.researches.big_transporter = true;
	var landTotal = 0;
	var navyTotal = 0;
	$.each(town.researches, function(k, v){
		var add = '';
		
		// Land
		if(unitKeys.land.indexOf(k) !== -1 && v) {
			if(!isUndefined(unitsBuilding[k])) {
				landTotal += parseInt(unitsBuilding[k]);
				add = '<span style="color: green;"> (+ ' + unitsBuilding[k] + ')</span>';
			}
			landTotal += parseInt(units[k]);
			availableUnitTypes.land[k] = '<span>' + units[k] + '</span>' + add;
		}
		
		// Navy
		if(unitKeys.navy.indexOf(k) !== -1 && v) {
			if(!isUndefined(unitsBuilding[k])) {
				navyTotal += parseInt(unitsBuilding[k]);
				add = '<span style="color: green;"> (+ ' + unitsBuilding[k] + ')</span>';
			}
			navyTotal += parseInt(units[k]);
			availableUnitTypes.navy[k] = '<span>' + units[k] + '</span>' + add;
		}
	});
	availableUnitTypes.land.total = landTotal;
	availableUnitTypes.navy.total = navyTotal;
	//console.log(availableUnitTypes);
	
	return availableUnitTypes;
}

function calTransport (townId, totalTroops, navyUnits) {
	var town = ownData.towns_info[townId];
	var researches = town.researches;
	var additionalSlot = 0;
	var defaultBigTransport = 26;
	var defaultSmallTransport = 10;
	var currentCapability = 0;
	var needCapability = 0;
	var needTransports = new Object();
	
	needTransports.big_transporter = 0;
	needTransports.small_transporter = 0;
	
	if(researches.shipwright) {
		additionalSlot = 6;
	}
	
	// Get current capability
	currentCapability = (parseInt($(navyUnits.big_transporter).text())*(defaultBigTransport + additionalSlot)) + 
						(parseInt($(navyUnits.small_transporter).text())*(defaultSmallTransport + additionalSlot));
						
	if (currentCapability < totalTroops) {
		needCapability = totalTroops - currentCapability;
		needTransports.big_transporter = Math.ceil(needCapability/(defaultBigTransport + additionalSlot), 0);
		needTransports.small_transporter = Math.ceil(needCapability/(defaultSmallTransport + additionalSlot), 0);
	}

	return needTransports;
}

function calMaxUnits (townId, type, unitName) {
	if(unitName === 'total') return true;

	resetUnitsCost();

	var town = ownData.towns_info[townId];
	var resInfo = {
		wood: town.resources.wood,
		stone: town.resources.stone,
		iron: town.resources.iron,
		population: town.available_population,
		favor: 0
	};
	var researches = town.researches;
	var unitCost = unitsCost[unitName];

	if(type === 'land') {
		// Land units
		if(researches.conscription) {
			unitCost.wood = unitCost.wood - (unitCost.wood*10/100);
			unitCost.stone = unitCost.stone - (unitCost.stone*10/100);
			unitCost.iron = unitCost.iron - (unitCost.iron*10/100);
		}
	} else {
		// Navy units
		if(researches.mathematics) {
			unitCost.wood = unitCost.wood - (unitCost.wood*10/100);
			unitCost.stone = unitCost.stone - (unitCost.stone*10/100);
			unitCost.iron = unitCost.iron - (unitCost.iron*10/100);
		}
	}

	var total = maxUnits(unitCost, resInfo);
	return total;
}

function resetUnitsCost () {
	unitsCost = {
		sword: {wood: 95, stone: 0, iron: 85, population: 1, favor: 0},
		archer: {wood: 120, stone: 0, iron: 75, population: 1, favor: 0},
		hoplite: {wood: 0, stone: 75, iron: 150, population: 1, favor: 0},
		slinger: {wood: 55, stone: 100, iron: 40, population: 1, favor: 0},
		rider: {wood: 240, stone: 120, iron: 360, population: 3, favor: 0},
		chariot: {wood: 200, stone: 440, iron: 320, population: 4, favor: 0},
		catapult: {wood: 700, stone: 700, iron: 700, population: 15, favor: 0},
		attack_ship: {wood: 1300, stone: 300, iron: 800, population: 10, favor: 0},
		bireme: {wood: 800, stone: 700, iron: 180, population: 8, favor: 0},
		trireme: {wood: 2000, stone: 1300, iron: 1300, population: 16, favor: 0},
		small_transporter: {wood: 800, stone: 0, iron: 400, population: 5, favor: 0},
		colonize_ship: {wood: 10000, stone: 10000, iron: 10000, population: 170, favor: 0},
		demolition_ship: {wood: 500, stone: 750, iron: 150, population: 8, favor: 0},
		big_transporter: {wood: 500, stone: 500, iron: 400, population: 7, favor: 0},
		centaur: {wood: 2300, stone: 900, iron: 400, population: 12, favor: 70},
		pegasus: {wood: 4000, stone: 1300, iron: 700, population: 20, favor: 120},
		envoy: {wood: 0, stone: 0, iron: 0, population: 3, favor: 12}
	};
}

function maxUnits (unitCost, resInfo) {
	var total = 0;
	var condition = true;
	
	while (condition && total <= 1000) {
		if ((unitCost.wood * total) > resInfo.wood || 
			(unitCost.stone * total) > resInfo.stone ||
			(unitCost.iron * total) > resInfo.iron ||
			(unitCost.population * total) > resInfo.population ||
			(unitCost.favor * total) > resInfo.favor) {
			condition = false;
		} else {
			total++;
		}
	}

	return total - 1;
}

function generateData(data) {
	var json = data.json;
	ownData = new Object();
	
	var collections = new Object();
	$.each(json.backbone.collections, function(i, o) {
		switch(o.class_name) {
			case "Towns":
				var towns = o.data;
			
				$.each(towns, function(j, v){
					var town = v.d;
					var townId = town.id;
					
					// Town info
					var townInfo = collections[townId];
					townInfo = setData(townInfo, "id", townId);
					townInfo = setData(townInfo, "name", town.name);
					townInfo = setData(townInfo, "god", town.god);
					townInfo = setData(townInfo, "storage", town.storage);
					townInfo = setData(townInfo, "available_population", town.available_population);
					townInfo = setData(townInfo, "conquest", town.conquest);
					townInfo = setData(townInfo, "has_conqueror", town.has_conqueror);
					townInfo = setData(townInfo, "espionage_storage", town.espionage_storage);
					townInfo = setData(townInfo, "resources_last_update", town.resources_last_update);
					
					// Resources info
					var resources = new Object();
					resources = setData(resources, "wood", town.resources.wood);
					resources = setData(resources, "stone", town.resources.stone);
					resources = setData(resources, "iron", town.resources.iron);
					
					// Add resources to town info
					townInfo = setData(townInfo, "resources", resources);

					// Add town info to own data
					setData(collections, townId, townInfo);
				});
				break;
			case "TownResearches":
				var townResearches = o.data;

				$.each(townResearches, function(j, v){
					var townResearche = v.d;
					var townId = townResearche.id;
					var townInfo = collections[townId];
					
					// Add researches to own data
					townInfo = setData(townInfo, "researches", townResearche);
					
					// Check transport slots
					if(townResearche.shipwright) {
						townInfo = setData(townInfo, "transport_slots", 26);
					} else {
						townInfo = setData(townInfo, "transport_slots", 20);
					}
					
					// Add town info to own data
					setData(collections, townId, townInfo);
				});
				break;
			case "BuildingOrders":
				var buildingOrders = o.data;
				
				$.each(buildingOrders, function(j, v){
					var buildingOrder = v.d;
					var townId = buildingOrder.town_id;
					var townInfo = collections[townId];
					
					//console.log(buildingOrder);
					
					// Add building order to own data
					var buildingInfo = new Object();
					buildingInfo = setData(buildingInfo, "name", buildingOrder.building_type);
					buildingInfo = setData(buildingInfo, "to_be_completed_at", buildingOrder.to_be_completed_at);
					
					var townBuildingOrder = setData(townInfo['building_orders'], buildingOrder.id, buildingInfo);
					
					townInfo = setData(townInfo, "building_orders", townBuildingOrder);
					setData(collections, townId, townInfo);
				});
				break;
			case "Units":
				var units = o.data;
				
				$.each(units, function(j, v){
					var unit = v.d;
					var currentTownId = unit.current_town_id;
					var homeTownId = unit.home_town_id;
					var townUnits = new Object();
					var unitTypes = [
						'archer', 'attack_ship', 'big_transporter', 'bireme', 'calydonian_boar', 'catapult',
						'centaur', 'cerberus', 'chariot', 'colonize_ship', 'demolition_ship', 'fury', 'griffin',
						'harpy', 'hoplite', 'manticore', 'medusa', 'minotaur', 'pegasus', 'rider', 'sea_monster',
						'slinger', 'small_transporter', 'sword', 'trireme', 'zyklop',
					];
					
					if(currentTownId === homeTownId) {
						var townInfo = collections[homeTownId];
						$.each(unit, function(u, n){
							if(unitTypes.indexOf(u) !== -1) {
								setData(townUnits, u, n);
							}
						});
						
						setData(townInfo, 'units', townUnits);
						setData(collections, homeTownId, townInfo);
					}
				});
				
				break;
			case "RemainingUnitOrders":
				var unitOrders = o.data;
			
				$.each(unitOrders, function(j, v){
					var unitOrder = v.d;
					var townId = unitOrder.town_id;
					var townInfo = collections[townId];
					
					// Add unit order to own data
					var unitInfo = new Object();
					unitInfo = setData(unitInfo, "name", unitOrder.unit_type);
					unitInfo = setData(unitInfo, "to_be_completed_at", unitOrder.to_be_completed_at);
					unitInfo = setData(unitInfo, "kind", unitOrder.kind);
					unitInfo = setData(unitInfo, "count", unitOrder.count);
					unitInfo = setData(unitInfo, "units_left", unitOrder.units_left);
					
					var townUnitOrder = setData(townInfo['unit_orders'], unitOrder.id, unitInfo);
					
					townInfo = setData(townInfo, "unit_orders", townUnitOrder);
					setData(collections, townId, townInfo);
				});
				
				
				break;
			default:
				return;
		}
	});
	
	// Add collections to ownData
	setData(ownData, 'towns_info', collections);
	
	// Notification
	setData(ownData, 'notifications', json.notifications);
}

function isUndefined (victim) {
	var tmpVariable = typeof victim;
	if (tmpVariable === "undefined"){
		return true;
	}
	
	return false;
}

function setData (obj, attr, value) {
	if(isUndefined(obj) || obj === null) {
		obj = new Object();
	}
	
	obj[attr] = value;
	return obj;
}

function getDate() {
	var m_names = new Array("Jan", "Feb", "Mar", 
		"Apr", "May", "Jun", "Jul", "Aug", "Sep", 
		"Oct", "Nov", "Dec");

	var d = new Date();
	var curr_date = d.getDate();
	var curr_month = d.getMonth();
	var curr_year = d.getFullYear();
	
	var dateTime = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + " "
		+ curr_date + "-" + m_names[curr_month] + "-" + curr_year;
	
	return dateTime;
}

function clearStorage() {
	localStorage.removeItem('ownData');
	localStorage.removeItem('last_update');
}

function showLoading() {
	$('.loading').css('display', '');
}
function hideLoading() {
	$('.loading').css('display', 'none');
}

function SortByName(a, b){
  var aName = a.name.toLowerCase();
  var bName = b.name.toLowerCase(); 
  return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}