var local = 'https://localhost:8000/';
var server = 'http://52.24.79.205/';
var socketUrl = local;
var townsInfo = {towns: {}};
var minCave = 100000;
var socket;
var serverTimeTimer;
var clientAction = 'clientToServer';

var unitTranslator = {
	archer: 'Archer',
	attack_ship: 'Light ship',
	catapult: 'Catapult',
	chariot: 'Chariot',
	colonize_ship: 'Colony ship',
	demolition_ship: 'Explosion ship',
	hoplite: 'Hoplite',
	rider: 'Horseman',
	slinger: 'Slinger',
	small_transporter: 'Small transporter',
	trireme: 'Trireme',
	sword: 'Sword',
	big_transporter: 'Big transporter',
	bireme: 'Bireme',
	ferkyon: 'Ferkyon',
	griffin: 'Griffin',
	harpy: 'Harpy',
	fury: 'Erinys',
	manticore: 'Manticore',
	godsent: 'Divine envoy',
};

var buildingTranslator = {
	academy: 'Academy',
	barracks: 'Barracks',
	docks: 'Harbor',
	farm: 'Farm',
	hide: 'Cave',
	ironer: 'Silver mine',
	library: 'Library',
	lighthouse: 'Lighthouse',
	lumber: 'Timber camp',
	main: 'Senate',
	market: 'Marketplace',
	oracle: 'Oracle',
	statue: 'Statue',
	stoner: 'Quarry',
	storage: 'Warehouse',
	temple: 'Temple',
	theater: 'Theater',
	thermal: 'Thermal baths',
	tower: 'Tower',
	wall: 'City wall',
	trade_office: 'Trade office'
};


var defaultSkipTowns = [
	73645, // Shadow 06
	84194, // Shadow 16
	107877, // Shadow 11
	101551, // Shadow 12
	92114, // Shadow 25
	97209, // Shadow 24
	111123, // Shadow 13
	77527, // Shadow 03
	85978, // Shadow 42
	107903, // Shadow 09
	82656 // Shadow 37
];

var oluTowns = [
	'Shadow 02',
	'Shadow 04',
	'Shadow 06',
	'Shadow 10',
	'Shadow 12',
	'Shadow 14',
	'Shadow 18',
	'Shadow 20',
	'Shadow 22',
	'Shadow 24',
	'Shadow 32',
	'Shadow 34'
];
var dluTowns = [
	'Shadow 01',
	'Shadow 03',
	'Shadow 09',
	'Shadow 15',
	'Shadow 19',
	'Shadow 21',
	'Shadow 27',
	'Shadow 29',
	'Shadow 33'
];
var lsTowns = [
	'Shadow 08',
	'Shadow 16',
	'Shadow 26',
	'Shadow 28',
	'Shadow 30'
];
var briTowns = [
	'Shadow 05',
	'Shadow 07',
	'Shadow 11',
	'Shadow 13',
	'Shadow 17',
	'Shadow 23',
	'Shadow 25',
	'Shadow 31'
];


window.onload = function() {
	showLoading(false);

	// Try to connect locally first
	socket = io.connect(local);
	initSocket();

	var checkSocketTimer = window.setInterval(function() {
		clearInterval(checkSocketTimer);

		if (!socket.connected) {
			console.log('Can not connect to local socket. Connect to server socket ...');
			socket.close();
			socket = io.connect(server);
			initSocket();
			socketUrl = server;
		}
	}, 3000);


	window.setInterval(function() {
		$('#butGetTowns').trigger('click');
	}, 60 * 60 * 1000); // 1 hour

	contentHeight();
};

function initSocket() {
	socket.on('ready', function(data) {
		$('.server-time').html('<span class="green">' + data.msg + '</span>');
		hideLoading();
	});

	socket.on('serverToClient', function(data) {
		var action = data.action;
		var result = data.data;

		switch (action) {
			case 'get_all_towns':
				townsInfo = result;

				setIsland();

				renderTowns(townsInfo);

				$('#butAutoFarm').css('display', '');
				$('input[name=chkSelFarmOption]').css('display', '');
				$('span[name=farmOption]').css('display', '');
				$("select[name=selFilter]").selectmenu({disabled: false});

				// Init server time
				clearInterval(serverTimeTimer);
				serverTimeTimer = window.setInterval(function() {
					townsInfo.serverTime += 1000;
					$('.server-time').html(timestampToString(townsInfo.serverTime));
				}, 1000);

				hideLoading();
				break;
			case 'towns_trade':
				townsInfo.towns = result.towns;

				tradeFillData(result.last_trade.from, result.last_trade.to);

				renderTowns(townsInfo);

				hideLoading();

				break;
			case 'festival':
				townsInfo.towns[result.town.id] = result.town;
				renderTowns(townsInfo);

				hideLoading();

				break;
			case 'festival_all':
				townsInfo.towns = result.towns;
				renderTowns(townsInfo);

				hideLoading();

				break;
			case 'log':
				var msg = $(result.message);

				addLog(result.message, result.time);

				var tr = $('<tr>');
				var time = $('<td class="time">');
				var content = $('<td class="content">');
				time.html(getPastTime(result.time));
				content.html(result.message);
				tr.append(time);
				tr.append(content);
				$('#txtLogs table').append(tr);

				$('#txtLogs').scrollTop($('#txtLogs')[0].scrollHeight);

				addLinkToLog();

				break;
			case 'build_units':
				townsInfo.towns[result.town.id] = result.town;
				renderTowns(townsInfo);

				hideLoading();

				break;
			case 'farm':
				hideLoading();
				break;
			case 'smart_farm':
				if (result.is_end) {
					hideLoading();
				}

				if (typeof result.town !== 'undefined') {
					townsInfo.towns[result.town.id] = result.town;
					renderTowns(townsInfo);
				}

				break;
			case 'goto':
				hideLoading();
				break;
			case 'script':
				console.log(result.result);
				break;
			case 'get_building_data':
				townsInfo.towns[result.building_data.id].building_data = result.building_data.building_data;

				renderBuildings(townsInfo.towns[result.building_data.id]);

				$("#dialogBuildings").dialog({
					autoOpen: false,
					show: {
						effect: "clip",
						duration: 500
					},
					hide: {
						effect: "clip",
						duration: 500
					},
					modal: true,
					width: 'auto'
				});
				$('#dialogBuildings').dialog("open");
				dialogModelClick($('#dialogBuildings'));

				hideLoading();

				break;
			case 'build_building':
				townsInfo.towns[result.town.id] = result.town;
				renderTowns(townsInfo);

				hideLoading();

				break;
			case 'update_cave':
				townsInfo.towns[result.town.id] = result.town;
				renderTowns(townsInfo);

				hideLoading();

				break;
			default:
				hideLoading();
				break;
		}
	});
}

$(document).ready(function() {
	initButtons();


	// Farm checkboxes
	$('input[name=chkSelFarmOption]').each(function()
	{
		$(this).change(function()
		{
			$('input[name=chkSelFarmOption]').prop('checked', false);
			$(this).prop('checked', true);
		});
	});
});

function initButtons() {
	// Init getData button event
	$('#butGetTowns').unbind('click');
	$('#butGetTowns').click(function() {
		showLoading(false);

		socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'get_all_towns', data: {}});
	});

	// Init butIntel button event
	$('#butIntel').unbind('click');
	$('#butIntel').click(function() {
		socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'intel_url', data: {}});
	});

	// Init butLogs button event
	$('#butLogs').unbind('click');
	$('#butLogs').click(function() {
		// Button clear
		$('#butClearLogs').unbind('click');
		$('#butClearLogs').click(function() {
			$('#txtLogs table').html('');
			localStorage.clear('logs');
		});

		$('#dialogLogs').dialog("open");
		dialogModelClick($('#dialogLogs'));

		// Get logs
		$('#txtLogs table').html('');
		var logs = getLogs();
		$.each(logs, function(i, log) {
			var tr = $('<tr>');
			var time = $('<td class="time">');
			var content = $('<td class="content">');

			time.html(getPastTime(log.time));
			content.html(log.text);
			tr.append(time);
			tr.append(content);

			$('#txtLogs table').append(tr);
		});
		addLinkToLog();
	});

	// Init butUnCheck button event
	$('#butUnCheck').unbind('click');
	$('#butUnCheck').click(function() {
		$('input[name=chkSelTown]').prop('checked', false);
	});

	// Init butFestivalAll button event
	$('#butFestivalAll').unbind('click');
	$('#butFestivalAll').click(function() {
		var townIds = {};
		var townsList = $('#tab-overview ul.towns-list').children('li');

		$.each(townsList.children('div.bg-green'), function() {
			if (!$(this).find('button[name=butFestival]').is(':disabled')) {
				var townId = $(this).parent().attr('data-id');
				townIds[townId] = townId;
			}
		});

		if (Object.keys(townIds).length <= 0) {
			alert('There is not any Green Town meets condition!');
		} else {
			showLoading(true);
			console.log(townIds);
			socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'festival_all', data: {townIds: townIds}});
		}
	});

	// Init butSmartFarm button event
	$('#butSmartFarm').unbind('click');
	$('#butSmartFarm').click(function() {
		showLoading(true);

		// Get cities don't have festival
		var towns = {};

		$.each(townsInfo.towns, function(id, town) {
			if ((typeof town.cultures === 'undefined' || typeof town.cultures.party === 'undefined')
					&& town.resources.population <= 0) {
				towns[id] = town.name;
			}
		});

		socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'smart_farm', data: {towns: towns}});
	});

	// Init butAutoFarm button event
	$('#butAutoFarm').unbind('click');
	$('#butAutoFarm').click(function() {
		// Get skipTowns
		var skipTowns = [];
		$.each($('input[name=chkSelTown]'), function() {
			if (!$(this).prop('checked') && $(this).is(':visible')) {
				skipTowns.push(parseInt($(this).attr('data-id')));
			}
		});

		// Get Farm options
		var farmOption = '5m';
		$.each($('input[name=chkSelFarmOption]'), function() {
			if ($(this).prop('checked') && $(this).is(':visible')) {
				farmOption = $(this).attr('value');
				return false;
			}
		});

		showLoading(true);

		socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'farm', data: {skipTowns: skipTowns, farmOption: farmOption}});
	});
}

function renderTowns(data) {
	var towns = data.towns;
	var townsList = $('#tab-overview ul.towns-list');
	var runningCelebration = 0;

	// Sort data before loop
	towns = $.map(towns, function(value, index) {
		return [value];
	});
	towns.sort(SortByName);

	townsList.html('');
	$.each(towns, function(townId, town) {
		renderTownBlock(town);

		// Check running celebration
		if (typeof town.cultures !== 'undefined') {
			runningCelebration += Object.keys(town.cultures).length;
		}

		// Add function for checkbox
		$('input[data-island=' + town.island_id + ']').each(function()
		{
			$(this).change(function()
			{
				if (!$(this).prop('checked')) {
					$(this).prop('checked', false);
				} else {
					$('input[data-island=' + town.island_id + ']').prop('checked', false);
					$(this).prop('checked', true);
				}
			});
		});
	});

	var dialogUnits = $('[aria-describedby=dialogUnits]');
	if (dialogUnits.length > 0 && dialogUnits.css('display') !== 'none') {
		var townId = parseInt(dialogUnits.find('button[name=butUnitBuild]').attr('data-id'));
		renderUnitsList(townsInfo.towns[townId]);
	}

	var dialogBuildings = $('[aria-describedby=dialogBuildings]');
	if (dialogBuildings.length > 0 && dialogBuildings.css('display') !== 'none') {
		var townId = parseInt(dialogBuildings.find('button[name=butUp]').attr('data-id'));
		renderBuildings(townsInfo.towns[townId]);
	}

	filter($("select[name=selFilter]").val());

	// Culture
	var culturalPoints = (townsInfo.cultural_points + runningCelebration) + ' (' + runningCelebration + ' running)';
	var nextCulturalPoints = 3 / 2 * ((townsInfo.cultural_level + 1) * (townsInfo.cultural_level + 1) - 3 * (townsInfo.cultural_level + 1) + 2);

	$('ul[name=statusBar] li[name="culturalPoints"]').children('span.value').html(culturalPoints + ' / ' + nextCulturalPoints);

	// Player Gods
	$.each($('ul[name=statusBar] li[name="playerGods"]'), function() {
		var godName = $(this).children('span.title').text().toLowerCase().replace(': ', '');
		$(this).children('span.value').html(townsInfo.player_gods[godName + '_favor']);
		if (townsInfo.player_gods[godName + '_favor'] >= 500) {
			$(this).children('span.value').addClass('red');
		} else {
			$(this).children('span.value').removeClass('red');
		}
	});

	//clearAllTimers();

	// Countdown
	$.each($('.party_count_down, .trade_count_down'), function() {
		if ($(this).children('input').val() > 0) {
			var target = $(this);
			var timer = window.setInterval(function() {
				target.children('input').val(parseInt(target.children('input').val()) - 1);

				var time = secondsToTime(target.children('input').val());

				if (time.h <= 0 && time.m <= 0 && time.s <= 0) {
					target.children('span').html('No');
					clearTimer(timer);
				} else {
					target.children('span').html(time.h + ':' + time.m + ':' + time.s);
				}
			}, 1000);
		}
	});
}

function renderTownBlock(town) {
	// Get sample HTML
	var sampleTownBlockHtml = $('#sampleTownsList ul.towns-list li:first').clone();
	sampleTownBlockHtml.css('display', 'none');

	// Add data-id and name
	sampleTownBlockHtml.attr('data-id', town.id);
	sampleTownBlockHtml.attr('data-name', town.name);

	// Fill city name
	var cityName = sampleTownBlockHtml.find('h4.header span');
	cityName.html(town.name + ' (' + town.resources.population + ')');

	cityName.unbind('click');
	cityName.click(function() {
		showLoading(false);

		socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'goto', data: {townId: town.id}});
	});

	if (town.resources.population <= 0) {
		if (town.resources.wood >= 15000 && town.resources.stone >= 18000 && town.resources.iron >= 15000) {
			sampleTownBlockHtml.children('div:first').removeClass('bg-white');
			sampleTownBlockHtml.children('div:first').addClass('bg-green');
		} else {
			sampleTownBlockHtml.children('div:first').removeClass('bg-white');
			sampleTownBlockHtml.children('div:first').addClass('bg-blue');
			if (typeof town.cultures !== 'undefined' && typeof town.cultures.party !== 'undefined') {
				sampleTownBlockHtml.children('div:first').removeClass('bg-blue');
				sampleTownBlockHtml.children('div:first').addClass('bg-orange');
			}
		}
	} else if (town.resources.population >= 200) {
		sampleTownBlockHtml.children('div:first').removeClass('bg-white');
		sampleTownBlockHtml.children('div:first').addClass('bg-pink');
	}

	// Fill townId into checkbox
	// This is used for Auto farm function
	var chkCity = sampleTownBlockHtml.find('h4.header input');
	chkCity.attr('data-id', town.id);
	chkCity.attr('data-island', town.island_id);
	chkCity.prop('checked', true);
	if (defaultSkipTowns.indexOf(town.id) !== -1) {
		chkCity.prop('checked', false);
	}

	// Fill res-value
	var resBar = sampleTownBlockHtml.find('ul[name="resBar"]');
	var resBarTrade = sampleTownBlockHtml.find('ul[name="resBarTrade"]');
	var lastTrade = getLastTrade(town.id);
	$.each(resBar.children('li'), function() {
		// Wood
		if ($(this).children('span.title').text().indexOf("W") > -1) {
			$(this).children('span.res-value').text(town.resources.wood);
			if (town.resources.wood >= town.resources.storage) {
				$(this).children('span.res-value').addClass('red');
			}

			resBarTrade.children('li').children('span.title:contains(W)').next().text(town.resources.wood + lastTrade.wood);
		}
		// Stone
		if ($(this).children('span.title').text().indexOf("S") > -1) {
			$(this).children('span.res-value').text(town.resources.stone);
			if (town.resources.stone >= town.resources.storage) {
				$(this).children('span.res-value').addClass('red');
			}

			resBarTrade.children('li').children('span.title:contains(S)').next().text(town.resources.stone + lastTrade.stone);
		}
		// Iron
		if ($(this).children('span.title').text().indexOf("I") > -1) {
			$(this).children('span.res-value').text(town.resources.iron);
			if (town.resources.iron >= town.resources.storage) {
				$(this).children('span.res-value').addClass('red');
			}

			resBarTrade.children('li').children('span.title:contains(I)').next().text(town.resources.iron + lastTrade.iron);
		}
	});

	if (lastTrade.wood <= 0 && lastTrade.stone <= 0 && lastTrade.iron <= 0) {
		resBarTrade.css('opacity', '0');
	}

	// Fill town-info
	var townInfo = sampleTownBlockHtml.find('ul[name="townInfo"]');
	$.each(townInfo.children('li'), function() {
		// Storage
		if ($(this).children('span.title').text().indexOf("Storage") > -1) {
			$(this).children('span.info-value').html(town.resources.storage);
		}
		// God
		if ($(this).children('span.title').text().indexOf("God") > -1) {
			$(this).children('span.info-value').html(town.god);
		}
		// Cave
		if ($(this).children('span.title').text().indexOf("Cave") > -1) {
			$(this).children('span.info-value').html(town.cave);
			if (parseInt(town.cave) <= minCave) {
				$(this).children('span.info-value').addClass('red');
			}

			$(this).children('span.title').unbind('click');
			$(this).children('span.title').click(function() {
				$('#dialogCave').dialog("open");
				dialogModelClick($('#dialogCave'));

				$('button[name=butCaveStore]').attr('disabled', 'disabled');
				$('button[name=butCaveStore]').addClass('disabled');

				$('input[name=txtCave]').val(town.cave);
				$('input[name=txtCave]').select();
				$('input[name=txtCave]').unbind('keyup');
				$('input[name=txtCave]').keyup(function(e) {
					if ($(this).val() <= town.cave) {
						$('button[name=butCaveStore]').attr('disabled', 'disabled');
						$('button[name=butCaveStore]').addClass('disabled');
					} else {
						$('button[name=butCaveStore]').removeClass('disabled');
						$('button[name=butCaveStore]').removeClass('ui-button-disabled');
						$('button[name=butCaveStore]').removeClass('ui-state-disabled');
						$('button[name=butCaveStore]').removeAttr('disabled');
					}
				});
				$('input[name=txtCave]').unbind('keydown');
				$('input[name=txtCave]').keydown(function(e) {
					// Allow: backspace, delete, tab, escape, enter and .
					if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
							(e.keyCode == 65 && e.ctrlKey === true) ||
							(e.keyCode >= 35 && e.keyCode <= 40)) {
						return;
					}
					// Ensure that it is a number and stop the keypress
					if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
						e.preventDefault();
					}
				});

				$('button[name=butCaveStore]').attr('data-id', town.id);
				$('button[name=butCaveStore]').unbind('click');
				$('button[name=butCaveStore]').click(function() {
					showLoading(false);
					socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'update_cave', data: {
							townId: parseInt($(this).attr('data-id')),
							target: parseInt($('input[name=txtCave]').val())
						}
					});
				});
			});
		}
		// Culture
		if ($(this).children('span.title').text().indexOf("Party") > -1) {
			if (typeof town.cultures !== 'undefined' && typeof town.cultures.party !== 'undefined') {
				var d = new Date(town.cultures.party * 1000);
				var secs = Math.floor((d.getTime() - Date.now()) / 1000);
				$(this).children('span.info-value').children('input').val(secs);
			}
		}
		// Last trade
		if ($(this).children('span.title').text().indexOf("Last trade") > -1) {
			if (typeof town.trades !== 'undefined') {
				var lastTrade = 0;
				$.each(town.trades, function(time, res) {
					if (time > lastTrade)
						lastTrade = time;
				});

				if (lastTrade > 0) {
					var d = new Date(lastTrade * 1000);
					var secs = Math.floor((d.getTime() - Date.now()) / 1000);
					$(this).children('span.info-value').children('input').val(secs);
				}
			}
		}
	});

	// Add data to button
	var buttons = sampleTownBlockHtml.find('button');
	buttons.attr('data-id', town.id);

	// Copy to towns list
	var townsList = $('#tab-overview ul.towns-list');
	townsList.append(sampleTownBlockHtml);


	// Button Units
	$('button[name=butUnits]').unbind('click');
	$('button[name=butUnits]').click(function() {
		var townId = $(this).attr('data-id');
		var town = townsInfo.towns[townId];

		// Replace title header
		var dialogUnits = $("#dialogUnits").dialog("instance");
		if (typeof dialogUnits !== 'undefined') {
			$("#dialogUnits").dialog("destroy");
		}
		$("#dialogUnits").attr('title', town.name + ' (' + town.resources.population + ')');

		// Add townId to button Build
		$("#dialogUnits").find('button[name=butUnitBuild]').attr('data-id', townId);
		$("#dialogUnits").find('button[name=butUnitBuild]').unbind('click');
		$("#dialogUnits").find('button[name=butUnitBuild]').click(function() {
			var townId = $(this).attr('data-id');
			var unitId = $(this).attr('data-unitId');
			var amount = $(this).prev('input[name=amount]').val();
			var cast = $('#dialogUnits').find('input[name=cast]');

			buildUnit(townId, unitId, amount, cast.prop('checked'));
		});

		// Input amount
		$("#dialogUnits").find('input[name=amount]').unbind('keydown');
		$("#dialogUnits").find('input[name=amount]').keydown(function(e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
					(e.keyCode == 65 && e.ctrlKey === true) ||
					(e.keyCode >= 35 && e.keyCode <= 40)) {
				return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}

			if (e.keyCode == 13) {
				var townId = $(this).next().attr('data-id');
				var unitId = $(this).next().attr('data-unitId');
				var amount = $(this).val();
				var cast = $('#dialogUnits').find('input[name=cast]');

				buildUnit(townId, unitId, amount, cast.prop('checked'));
			}
		});

		// Fill data
		renderUnitsList(town);


		$("#dialogUnits").dialog({
			autoOpen: false,
			show: {
				effect: "clip",
				duration: 500
			},
			hide: {
				effect: "clip",
				duration: 500
			},
			modal: true,
			width: 'auto'
		});
		$('#dialogUnits').dialog("open");
		dialogModelClick($('#dialogUnits'));
	});


	// Button Trade
	var butTrade = sampleTownBlockHtml.find('button[name=butTrade]');
	if (town.trade_capacity <= 0 || (town.resources.wood < 1000 && town.resources.stone < 1000 && town.resources.iron < 1000)) {
		butTrade.attr('disabled', 'disabled');
		butTrade.addClass('disabled');

		butTrade.parents('div.town-block').removeClass('bg-blue');
		butTrade.parents('div.town-block').removeClass('bg-white');
		butTrade.parents('div.town-block').addClass('bg-gray');
	} else {
		butTrade.unbind('click');
		butTrade.click(function() {
			renderTownsSelList($(this).attr('data-id'));

			if (typeof $("select[name=selTown]").selectmenu('instance') !== 'undefined')
			{
				$("select[name=selTown]").selectmenu("destroy");
			}
			$("select[name=selTown]").selectmenu({width: "auto"});

			$("select[name=selTown]").selectmenu({change: function(event, ui) {
					tradeFillData($("select[name=selTown].fromTown").val(), $("select[name=selTown].toTown").val());
				}});

			// Fill data
			tradeFillData($("select[name=selTown].fromTown").val(), $("select[name=selTown].toTown").val());

			// Button butTradeSend
			$('button[name=butTradeSend]').unbind('click');
			$('button[name=butTradeSend]').click(function() {
				showLoading(true);

				var tradeData = {};
				tradeData[parseInt($("select[name=selTown].fromTown").val())] = {
					from: parseInt($("select[name=selTown].fromTown").val()),
					to: parseInt($("select[name=selTown].toTown").val()),
					resources: {
						wood: parseInt($('#dialogTrade div.left').find('ul[name=resInput] li').eq(0).children('input').val()),
						stone: parseInt($('#dialogTrade div.left').find('ul[name=resInput] li').eq(1).children('input').val()),
						iron: parseInt($('#dialogTrade div.left').find('ul[name=resInput] li').eq(2).children('input').val())
					}
				};

				socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'towns_trade', data: {tradeData: tradeData}});
			});

			// Inputs
			$.each($('#dialogTrade div.left').find('ul[name=resInput] li').children('input'), function() {
				$(this).unbind('keypress');
				$(this).keypress(function(e) {
					if (e.keyCode == 13 && !$('#dialogTrade').find('button[name=butTradeSend]').hasClass('disabled')) {
						$('#dialogTrade').find('button[name=butTradeSend]').button().trigger('click');
					}
				});

				$(this).unbind('keydown');
				$(this).keydown(function(e) {
					if (e.keyCode === 9) {
						if (parseInt($(this).attr('tabindex')) === 3) {
							$('#dialogTrade div.left').find('ul[name=resInput] li').children('input[tabindex=1]').select();
							e.preventDefault();
						}
					}
				});

				$(this).unbind('keyup');
				$(this).keyup(function(e) {
					if (e.keyCode !== 9) {
						tradeInputChange();
					}
				});
			});


			$("#dialogTrade").dialog({
				autoOpen: false,
				show: {
					effect: "clip",
					duration: 500
				},
				hide: {
					effect: "clip",
					duration: 500
				},
				modal: true,
				width: 'auto'
			});
			$('#dialogTrade').dialog("open", function() {
				alert('ok');
			});
			dialogModelClick($('#dialogTrade'));
		});
	}

	// Button Festival
	var butFestival = sampleTownBlockHtml.find('button[name=butFestival]');
	if (sampleTownBlockHtml.children('div.town-block').hasClass('bg-green') && (typeof town.cultures === 'undefined' || typeof town.cultures.party === 'undefined')) {
		butFestival.unbind('click');
		butFestival.click(function() {
			showLoading(true);

			socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'festival', data: {townId: town.id}});
		});
	} else {
		butFestival.attr('disabled', 'disabled');
		butFestival.addClass('disabled');
	}

	// Button Building
	var butBuilding = sampleTownBlockHtml.find('button[name=butBuildings]');
	butBuilding.attr('data-id', town.id);
	butBuilding.unbind('click');
	butBuilding.click(function() {
		showLoading(true);

		// Send request to server
		socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'get_building_data', data: {townId: town.id}});
	});

	// Button Auto Trade
	var butAutoTrade = sampleTownBlockHtml.find('button[name=butAutoTrade]');
	butAutoTrade.unbind('click');
	butAutoTrade.click(function() {
		showLoading(true);

		socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'auto_trade', data: {townId: town.id}});
	});

	// Button Smart Farm
	var butTownSmartFarm = sampleTownBlockHtml.find('button[name=butTownSmartFarm]');
	butTownSmartFarm.unbind('click');
	butTownSmartFarm.click(function() {
		showLoading(true);

		var towns = {};
		towns[town.id] = town.name;

		socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'smart_farm', data: {towns: towns}});
	});
}

function renderBuildings(town) {
	var sampleBuildings = $('#sampleBuildings').clone();
	var table = sampleBuildings.children('table');
	var buildingData = town.building_data;
	console.log(buildingData);

	var special1 = {
		thermal: 'thermal',
		lighthouse: 'lighthouse'
	};
	var special2 = {
		tower: 'tower',
		statue: 'statue'
	};

	var orders = Object.keys(town.buildings_order).length;

	$.each(buildingTranslator, function(name, translation) {
		var building = buildingData[name];

		var color = '';
		if (building.level === building.next_level) {
			color = 'class="gray"';
		} else if (building.level < building.next_level) {
			if (town.resources.wood >= building.resources_for.wood &&
					town.resources.stone >= building.resources_for.stone &&
					town.resources.iron >= building.resources_for.iron &&
					town.resources.population >= building.population_for) {
				color = 'class="green click-able"';
			}
		}

		var buildingOrder = 0;
		$.each(town.buildings_order, function(orderId, orderInfo) {
			if (orderInfo.building_type === name && !orderInfo.tear_down) {
				buildingOrder++;
			}

			if (orderInfo.building_type === name && orderInfo.tear_down) {
				buildingOrder--;
			}

			if (orderInfo.building_type === name && typeof special1[orderInfo.building_type] !== 'undefined') {
				table.find('td[data-name=special1]').html('<span data-name="' + orderInfo.building_type + '" ' + color + '>' + translation + ' ' + buildingOrder + '</span>');
			}

			if (orderInfo.building_type === name && typeof special2[orderInfo.building_type] !== 'undefined') {
				table.find('td[data-name=special2]').html('<span data-name="' + orderInfo.building_type + '" ' + color + '>' + translation + ' ' + buildingOrder + '</span>');
			}
		});
		if (buildingOrder === 0) {
			buildingOrder = '';
		} else if (buildingOrder > 0) {
			buildingOrder = '+' + buildingOrder;
		}

		table.find('td[data-name=' + name + ']').html('<span data-name="' + name + '" ' + color + '>' + translation + ' (' + town.buildings[name] + ') ' + buildingOrder + '</span>');

		if (typeof special1[name] !== 'undefined' && town.buildings[name] > 0) {
			table.find('td[data-name=special1]').html('<span data-name="' + name + '" ' + color + '>' + translation + ' ' + buildingOrder + '</span>');
		}

		if (typeof special2[name] !== 'undefined' && town.buildings[name] > 0) {
			table.find('td[data-name=special2]').html('<span data-name="' + name + '" ' + color + '>' + translation + ' ' + buildingOrder + '</span>');
		}
	});

	$.each(sampleBuildings.find('span.click-able'), function() {
		$(this).unbind('click');
		$(this).click(function() {
			sampleBuildings.find('span[name=buildingBuild]').html(buildingTranslator[$(this).attr('data-name')]);
			sampleBuildings.find('button[name=butUp]').attr('data-building-name', $(this).attr('data-name'));
			sampleBuildings.find('button[name=butDown]').attr('data-building-name', $(this).attr('data-name'));
		});
	});

	sampleBuildings.find('button[name=butUp]').attr('data-id', town.id);
	sampleBuildings.find('button[name=butUp]').unbind('click');
	sampleBuildings.find('button[name=butUp]').click(function() {
		if (sampleBuildings.find('span[name=buildingBuild]').text() === '') {
			alert('Please select at least 1 building');
		} else {
			showLoading(true);

			socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'build_building', data: {
					name: $(this).attr('data-building-name')
				}
			});
		}
	});

	sampleBuildings.find('button[name=butDown]').attr('data-id', town.id);
	sampleBuildings.find('button[name=butDown]').unbind('click');
	sampleBuildings.find('button[name=butDown]').click(function() {
		if (sampleBuildings.find('span[name=buildingBuild]').text() === '') {
			alert('Please select at least 1 building');
		} else {
			alert('Down');
		}
	});

	sampleBuildings.find('div[name=orders] span').html('Orders: ' + orders + '/7');

	$('#dialogBuildings div').html('');
	$('#dialogBuildings div').append(sampleBuildings);
}

function renderUnitsList(town) {
	var sampleUnitsList = $('#sampleUnitsList').clone();
	var landList = sampleUnitsList.find('ul[name=land]');
	var navyList = sampleUnitsList.find('ul[name=navy]');
	var li = landList.find('li:first');
	var availableUnits = getAvailableUnits(town);
	// console.log(hardBuildUnits);


	// Land
	landList.html('');
	sampleUnitsList.find('h4[name=land]').html(sampleUnitsList.find('h4[name=land]').html() + ' (' + (7 - town.units_order.land_slots) + '/7)');
	$.each(town.units.land, function(name, value) {
		// Don't list hero here
		if (name === town.hero || name in getHeroes()) {
			return true;
		}

		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}

		var liLand = li.clone();

		// Get units order
		if (town.units_order.land[name] > 0) {
			value = value + '<span class="green"> (' + town.units_order.land[name] + ')</span>';
		}

		// Get units can built
		var totalCanBuilt = unitsCanBuilt(town, name, 'land');
		if (totalCanBuilt > 0) {
			value = value + '<span class="red"> (' + totalCanBuilt + ')</span>';
		}

		liLand.find('span.title').html(translate(name, unitTranslator) + ': ');
		liLand.find('span.unit-value').html(value);
		liLand.attr('data-build', totalCanBuilt);
		liLand.attr('data-unitId', name);

		landList.append(liLand);
	});
	$.each(town.units_order.land, function(name, value) {
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}


		var liLand = li.clone();

		if (!(name in town.units.land)) {
			value = '<span class="green">' + value + '</span>';

			// Get units can built
			var totalCanBuilt = unitsCanBuilt(town, name, 'land');
			if (totalCanBuilt > 0) {
				value = value + '<span class="red"> (' + totalCanBuilt + ')</span>';
			}

			liLand.find('span.title').html(translate(name, unitTranslator) + ': ');
			liLand.find('span.unit-value').html(value);
			liLand.attr('data-build', totalCanBuilt);
			liLand.attr('data-unitId', name);

			landList.append(liLand);
		}
	});
	$.each(availableUnits.land, function(index, name) {
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}


		var liLand = li.clone();
		var totalCanBuilt = unitsCanBuilt(town, name, 'land');

		if (!(name in town.units.land) && !(name in town.units_order.land)) {
			var value = '<span class="red">' + totalCanBuilt + '</span>';

			liLand.find('span.title').html(translate(name, unitTranslator) + ': ');
			liLand.find('span.unit-value').html(value);
			liLand.attr('data-build', totalCanBuilt);
			liLand.attr('data-unitId', name);

			landList.append(liLand);
		}
	});

	// Navy
	navyList.html('');
	sampleUnitsList.find('h4[name=navy]').html(sampleUnitsList.find('h4[name=navy]').html() + ' (' + (7 - town.units_order.navy_slots) + '/7)');
	$.each(town.units.navy, function(name, value) {
		// Don't list hero here
		if (name === town.hero || name in getHeroes()) {
			return true;
		}

		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}

		// Skip if militia
		if (name === 'militia') {
			return true;
		}


		var liNavy = li.clone();

		// Get units order
		if (town.units_order.navy[name] > 0) {
			value = value + '<span class="green"> (' + town.units_order.navy[name] + ')</span>';
		}

		// Get units can built
		var totalCanBuilt = unitsCanBuilt(town, name, 'navy');
		if (totalCanBuilt > 0) {
			value = value + '<span class="red"> (' + totalCanBuilt + ')</span>';
		}

		liNavy.find('span.title').html(translate(name, unitTranslator) + ': ');
		liNavy.find('span.unit-value').html(value);
		liNavy.attr('data-build', totalCanBuilt);
		liNavy.attr('data-unitId', name);

		navyList.append(liNavy);
	});
	$.each(town.units_order.navy, function(name, value) {
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}


		var liNavy = li.clone();

		if (!(name in town.units.navy)) {
			value = '<span class="green">' + value + '</span>';

			// Get units can built
			var totalCanBuilt = unitsCanBuilt(town, name, 'navy');
			if (totalCanBuilt > 0) {
				value = value + '<span class="red"> (' + totalCanBuilt + ')</span>';
			}

			liNavy.find('span.title').html(translate(name, unitTranslator) + ': ');
			liNavy.find('span.unit-value').html(value);
			liNavy.attr('data-build', totalCanBuilt);
			liNavy.attr('data-unitId', name);

			navyList.append(liNavy);
		}
	});
	$.each(availableUnits.navy, function(index, name) {
		// Skip if unit is not in hardBuildUnits
		if (!isBuildUnit(town.id, name)) {
			return true;
		}


		var liNavy = li.clone();
		var totalCanBuilt = unitsCanBuilt(town, name, 'navy');

		if (!(name in town.units.navy) && !(name in town.units_order.navy)) {
			var value = '<span class="red">' + totalCanBuilt + '</span>';

			liNavy.find('span.title').html(translate(name, unitTranslator) + ': ');
			liNavy.find('span.unit-value').html(value);
			liNavy.attr('data-build', totalCanBuilt);
			liNavy.attr('data-unitId', name);

			navyList.append(liNavy);
		}
	});

	// Transports need
	var transportsNeed = calTransportsNeed(town, sampleUnitsList);
	var bunks = '<span class="red">No</span>';
	if (town.researches.berth) {
		bunks = 'Yes';
	}
	if (sampleUnitsList.children('span.title').text().indexOf("Bunks") > -1) {
		sampleUnitsList.children('span[name=bunks]').html(bunks);
	}

	var transportsNeedHtml = sampleUnitsList.find('ul[name="transportsNeed"]');
	$.each(transportsNeedHtml.children('li'), function() {
		// Big transporter
		if ($(this).children('span.title').text().indexOf("Big") > -1) {
			$(this).children('span.unit-value').text('+' + transportsNeed.big_transporter);
		}
		// Small transporter
		if ($(this).children('span.title').text().indexOf("Small") > -1) {
			$(this).children('span.unit-value').text('+' + transportsNeed.small_transporter);
		}

		$(this).children('span.unit-value').addClass('red');
	});


	// Reset input data
	$('#dialogUnits').find('span[name=units]').html('');
	$('#dialogUnits').find('input').val('');
	$('#dialogUnits').find('button[name=butUnitBuild]').attr('data-unitId', '');


	// Reset population
	$('.ui-dialog-title').text(town.name + ' (' + town.resources.population + ')');


	// Append
	$('#dialogUnits').children('div.units-list').html('');
	$('#dialogUnits').children('div.units-list').append(sampleUnitsList);
}

function isBuildUnit(townId, name) {
	var hardBuildUnits = getHardBuildUnits();

	if (typeof hardBuildUnits[townId] !== 'undefined') {
		var getHardBuildUnit = hardBuildUnits[townId];
		if (typeof getHardBuildUnit[name] === 'undefined') {
			return false;
		}
	}

	return true;
}

function unitClick(li) {
	var unitsCost = getUnitsCost();
	var totalUnits = unitsCost[li.attr('data-unitid')].population * li.attr('data-build');

	$('#dialogUnits').find('span[name=units]').html(unitTranslator[li.attr('data-unitid')] + '(' + totalUnits + ')');
	$('#dialogUnits').find('input').val(li.attr('data-build'));
	$('#dialogUnits').find('button[name=butUnitBuild]').attr('data-unitId', li.attr('data-unitid'));
}

function buildUnit(townId, unitId, amount, cast) {
	if (typeof townId === 'undefined' || townId === '') {
		$('#dialogUnits').find('.error').html('Error: TownId is undefined!');
		$('#dialogUnits').find('.error').fadeIn(500);
		return false;
	}
	if (typeof unitId === 'undefined' || unitId === '') {
		$('#dialogUnits').find('.error').html('Please select a unit!');
		$('#dialogUnits').find('.error').fadeIn(500);
		return false;
	}
	if (typeof amount === 'undefined' || amount === '' || amount == 0) {
		$('#dialogUnits').find('.error').html('Please enter the amount!');
		$('#dialogUnits').find('.error').fadeIn(500);
		return false;
	}

	// Check if slot = 0
	var unitsCost = getUnitsCost();
	if (unitsCost[unitId].type === 'land') {
		// Land
		if (townsInfo.towns[townId].units_order.land_slots <= 0) {
			$('#dialogUnits').find('.error').html('Land units slots are full!');
			$('#dialogUnits').find('.error').fadeIn(500);
			return false;
		}
	} else {
		// Navy
		if (townsInfo.towns[townId].units_order.navy_slots <= 0) {
			$('#dialogUnits').find('.error').html('Navy units slots are full!');
			$('#dialogUnits').find('.error').fadeIn(500);
			return false;
		}
	}


	$('#dialogUnits').find('.error').fadeOut(500);

	showLoading(true);

	socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'build_units', data: {
			townId: townId,
			unitName: unitId,
			amount: amount,
			cast: cast,
			type: unitsCost[unitId].type
		}
	});
}

function getAvailableUnits(town) {
	var availableUnits = {};
	availableUnits.land = ['sword'];
	availableUnits.navy = ['big_transporter'];

	var researches = town.researches;
	var unitsCost = getUnitsCost();

	$.each(unitsCost, function(name, attr) {
		if (researches[name]) {
			if (attr.type === 'land') {
				availableUnits.land.push(name);
			} else {
				availableUnits.navy.push(name);
			}
		}
	});

	if (typeof town.god !== 'undefined' && town.god !== '') {
		availableUnits.land.push('godsent');
	}

	return availableUnits;
}

function calTransportsNeed(town, sampleUnitsList) {
	var researches = town.researches;
	var additionalSlot = 0;
	var defaultBigTransport = 26;
	var defaultSmallTransport = 10;
	var currentCapability = 0;
	var needCapability = 0;
	var needTransports = {};

	needTransports.big_transporter = 0;
	needTransports.small_transporter = 0;

	if (researches.berth) {
		additionalSlot = 6;
	}

	// Get total transporter
	var bigTransporters = town.units.navy.big_transporter;
	var smallTransporters = town.units.navy.small_transporter;
	if (typeof bigTransporters === 'undefined') {
		bigTransporters = 0;
	}
	if (typeof smallTransporters === 'undefined') {
		smallTransporters = 0;
	}

	if (typeof town.units_order.navy.big_transporter !== 'undefined') {
		bigTransporters += town.units_order.navy.big_transporter;
	}
	if (typeof town.units_order.navy.small_transporter !== 'undefined') {
		smallTransporters += town.units_order.navy.small_transporter;
	}

	// Get current capability
	currentCapability = (parseInt(bigTransporters) * (defaultBigTransport + additionalSlot)) +
			(parseInt(smallTransporters) * (defaultSmallTransport + additionalSlot));

	// Get total land units
	var landUnits = town.units.land;
	var landUnitsOrder = town.units_order.land;
	var unitsCost = getUnitsCost();
	var totalLandUnits = 0;
	$.each(landUnits, function(name, value) {
		if (unitsCost[name].characteristic === 'ground') {
			totalLandUnits += value * unitsCost[name].population;
		}
	});
	$.each(landUnitsOrder, function(name, value) {
		if (unitsCost[name].characteristic === 'ground') {
			totalLandUnits += value * unitsCost[name].population;
		}
	});

	//console.log(totalLandUnits);

	if (currentCapability < totalLandUnits) {
		needCapability = totalLandUnits - currentCapability;
		needTransports.big_transporter = Math.ceil(needCapability / (defaultBigTransport + additionalSlot), 0);
		needTransports.small_transporter = Math.ceil(needCapability / (defaultSmallTransport + additionalSlot), 0);
	} else {
		sampleUnitsList.find('span[name=freeSlots]').text(currentCapability - totalLandUnits);
	}

	return needTransports;
}

function filter(code) {
	var townsList = $('#tab-overview ul.towns-list').children('li');
	var i = 0;

	switch (code) {
		case 'white': // White towns
			townsList.fadeOut(500, function() {
				i++;

				if (i === Object.keys(townsInfo.towns).length) {
					townsList.children('div.bg-white').parent().fadeIn(500);
				}
			});

			break;
		case 'green': // Green towns
			townsList.fadeOut(500, function() {
				i++;

				if (i === Object.keys(townsInfo.towns).length) {
					townsList.children('div.bg-green').parent().fadeIn(500);
				}
			});

			break;
		case 'blue': // Blue towns
			townsList.fadeOut(500, function() {
				i++;

				if (i === Object.keys(townsInfo.towns).length) {
					townsList.children('div.bg-blue').parent().fadeIn(500);
				}
			});

			break;
		case 'red': // Red towns
			townsList.fadeOut(500, function() {
				i++;

				if (i === Object.keys(townsInfo.towns).length) {
					townsList.children('div.bg-pink').parent().fadeIn(500);
				}
			});

			break;
		case 'olu': // OLU nuke
			townsList.fadeOut(500, function() {
				i++;

				if (i === Object.keys(townsInfo.towns).length) {
					$.each(oluTowns, function(index, value) {
						townsList.parent().children('li[data-name="' + value + '"]').fadeIn(500);
					});
				}
			});

			break;
		case 'dlu': // DLU nuke
			townsList.fadeOut(500, function() {
				i++;

				if (i === Object.keys(townsInfo.towns).length) {
					$.each(dluTowns, function(index, value) {
						townsList.parent().children('li[data-name="' + value + '"]').fadeIn(500);
					});
				}
			});

			break;
		case 'ls': // LS nuke
			townsList.fadeOut(500, function() {
				i++;

				if (i === Object.keys(townsInfo.towns).length) {
					$.each(lsTowns, function(index, value) {
						townsList.parent().children('li[data-name="' + value + '"]').fadeIn(500);
					});
				}
			});

			break;
		case 'bri': // Bri nuke
			townsList.fadeOut(500, function() {
				i++;

				if (i === Object.keys(townsInfo.towns).length) {
					$.each(briTowns, function(index, value) {
						townsList.parent().children('li[data-name="' + value + '"]').fadeIn(500);
					});
				}
			});

			break;
		default: // All
			townsList.fadeIn(500);
			break;
	}
}

function dialogModelClick(dialog) {
	$('div.ui-widget-overlay.ui-front').unbind('click');
	$('div.ui-widget-overlay.ui-front').click(function() {
		dialog.dialog("close");
	});
}

function unitsCanBuilt(town, unitName, type) {
	var researches = town.researches;
	var unitCost = getUnitsCost()[unitName];

	if (type === 'land') {
		// Land units
		if (researches.conscription) {
			unitCost.wood = unitCost.wood - (unitCost.wood * 10 / 100);
			unitCost.stone = unitCost.stone - (unitCost.stone * 10 / 100);
			unitCost.iron = unitCost.iron - (unitCost.iron * 10 / 100);
		}
	} else {
		// Navy units
		if (researches.mathematics) {
			unitCost.wood = unitCost.wood - (unitCost.wood * 10 / 100);
			unitCost.stone = unitCost.stone - (unitCost.stone * 10 / 100);
			unitCost.iron = unitCost.iron - (unitCost.iron * 10 / 100);
		}
	}


	// Calculate total
	var total = 0;
	var condition = true;

	while (condition && total <= 1000) {
		if ((unitCost.wood * total) > town.resources.wood ||
				(unitCost.stone * total) > town.resources.stone ||
				(unitCost.iron * total) > town.resources.iron ||
				(unitCost.population * total) > town.resources.population ||
				(unitCost.favor * total) > town.resources.favor) {
			condition = false;
		} else {
			total++;
		}
	}

	return total - 1;
}

function renderTownsSelList(fromTownId) {
	$('select[name=selTown]').html('');
	var towns = townsInfo.towns;

	towns = $.map(towns, function(value, index) {
		return [value];
	});
	towns.sort(SortByName);

	$.each(towns, function(townId, town) {
		$('select[name=selTown]').append('<option value="' + town.id + '">' + town.name + '</option>');
	});


	$.each($('select[name=selTown].fromTown').children('option'), function() {
		if ($(this).val() == fromTownId) {
			$(this).attr('selected', 'selected');
		}
	});
}

function tradeFillData(fromTownId, toTownId) {
	var fromTown = townsInfo.towns[fromTownId];
	var toTown = townsInfo.towns[toTownId];

	var fromBlock = $('#dialogTrade div.left');
	var toBlock = $('#dialogTrade div.right');
	var defInput = getDefTrade(fromTown.trade_capacity);
	var lastTrade = getLastTrade(toTownId);

	// ResBar
	$.each(fromBlock.find('ul[name=resBar] li'), function() {
		// Wood
		if ($(this).children('span.title').text().indexOf("W") > -1) {
			$(this).children('span.res-value').text(fromTown.resources.wood);

			if (fromTown.resources.wood >= defInput)
				fromBlock.find('ul[name=resInput] li').children('span:contains(W)').next().val(defInput);
			else
				fromBlock.find('ul[name=resInput] li').children('span:contains(W)').next().val(fromTown.resources.wood);
		}
		// Stone
		if ($(this).children('span.title').text().indexOf("S") > -1) {
			$(this).children('span.res-value').text(fromTown.resources.stone);

			if (fromTown.resources.stone >= defInput)
				fromBlock.find('ul[name=resInput] li').children('span:contains(S)').next().val(defInput);
			else
				fromBlock.find('ul[name=resInput] li').children('span:contains(S)').next().val(fromTown.resources.stone);
		}
		// Iron
		if ($(this).children('span.title').text().indexOf("I") > -1) {
			$(this).children('span.res-value').text(fromTown.resources.iron);

			if (fromTown.resources.iron >= defInput)
				fromBlock.find('ul[name=resInput] li').children('span:contains(I)').next().val(defInput);
			else
				fromBlock.find('ul[name=resInput] li').children('span:contains(I)').next().val(fromTown.resources.iron);
		}
	});
	$.each(toBlock.find('ul[name=resBar] li'), function() {
		// Wood
		if ($(this).children('span.title').text().indexOf("W") > -1) {
			$(this).children('span.res-value').text(toTown.resources.wood);

			// Fill input
			var newWood = parseInt(toTown.resources.wood) + parseInt(lastTrade.wood) + parseInt(fromBlock.find('ul[name=resInput] li').eq(0).children('input').val());
			toBlock.find('ul[name=resInput] li').eq(0).children('input').val(newWood);
		}
		// Stone
		if ($(this).children('span.title').text().indexOf("S") > -1) {
			$(this).children('span.res-value').text(toTown.resources.stone);

			// Fill input
			var newStone = parseInt(toTown.resources.stone) + parseInt(lastTrade.stone) + parseInt(fromBlock.find('ul[name=resInput] li').eq(1).children('input').val());
			toBlock.find('ul[name=resInput] li').eq(1).children('input').val(newStone);
		}
		// Iron
		if ($(this).children('span.title').text().indexOf("I") > -1) {
			$(this).children('span.res-value').text(toTown.resources.iron);

			// Fill input
			var newIron = parseInt(toTown.resources.iron) + parseInt(lastTrade.iron) + parseInt(fromBlock.find('ul[name=resInput] li').eq(2).children('input').val());
			toBlock.find('ul[name=resInput] li').eq(2).children('input').val(newIron);
		}
	});

	// Capacity & Storage
	$.each(fromBlock.find('ul[name=townInfo] li'), function() {
		// Capacity
		if ($(this).children('span.title').text().indexOf("Capacity") > -1) {
			$(this).children('span.info-value').html(fromTown.trade_capacity);
			$(this).children('input[name=tradeCapacity]').val(fromTown.trade_capacity);
		}
	});
	$.each(toBlock.find('ul[name=townInfo] li'), function() {
		// Storage
		if ($(this).children('span.title').text().indexOf("Storage") > -1) {
			$(this).children('span.info-value').html(toTown.resources.storage);
		}
	});

	// Inputs
	fromBlock.find('ul[name=resInput] li').children('input').eq(0).select();

	tradeResClick(fromTown.trade_capacity);

	tradeInputChange();
}

function getLastTrade(townId) {
	var lastCapacity = {
		wood: 0,
		stone: 0,
		iron: 0
	};

	var trades = townsInfo.towns[townId].trades;

	if (trades !== null && typeof trades !== 'undefined') {
		$.each(trades, function(completeAt, resources) {
			lastCapacity.wood += resources.wood;
			lastCapacity.stone += resources.stone;
			lastCapacity.iron += resources.iron;
		});
	}

	return lastCapacity;
}

function tradeResClick(capacity) {
	var fromBlock = $('#dialogTrade div.left');

	$.each(fromBlock.find('ul[name=resInput] li').children('span'), function() {
		$(this).unbind('click');
		$(this).click(function() {
			if (parseInt($(this).next().val()) === 0) {
				if ($(this).text().indexOf("W") > -1) {
					if (parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(W)').next().text()) <= capacity)
						$(this).next().val(parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(W)').next().text()));
					else
						$(this).next().val(capacity);
				}

				if ($(this).text().indexOf("S") > -1) {
					if (parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(S)').next().text()) <= capacity)
						$(this).next().val(parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(S)').next().text()));
					else
						$(this).next().val(capacity);
				}

				if ($(this).text().indexOf("I") > -1) {
					if (parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(I)').next().text()) <= capacity)
						$(this).next().val(parseInt(fromBlock.find('ul[name=resBar] li').children('span.title:contains(I)').next().text()));
					else
						$(this).next().val(capacity);
				}
			} else {
				$(this).next().val('0');
			}

			tradeInputChange();
		});
	});

	fromBlock.find('ul[name=townInfo] li span:contains(Capacity)').unbind('click');
	fromBlock.find('ul[name=townInfo] li span:contains(Capacity)').click(function() {
		var defInput = getDefTrade(capacity);

		fromBlock.find('ul[name=resInput] li').children('input').val(defInput);
	});
}

function getDefTrade(capacity) {
	return Math.floor(capacity / 3);
}

function tradeInputChange() {
	if ($("select[name=selTown].toTown").val() === '0' || $("select[name=selTown].fromTown").val() === '0') {
		return false;
	}

	var fromBlock = $('#dialogTrade div.left');
	var toBlock = $('#dialogTrade div.right');
	var butTradeSend = $('#dialogTrade').find('button[name=butTradeSend]');
	var lastTrade = getLastTrade($("select[name=selTown].toTown").val());
	var storage = parseInt(toBlock.find('ul[name=townInfo] li:first span.info-value').text());

	$.each(fromBlock.find('ul[name=resInput] li').children('input'), function() {
		if (isNaN(parseInt($(this).val())))
			$(this).val('0');
		else
			$(this).val(parseInt($(this).val()));
	});

	var newWood = parseInt(fromBlock.find('ul[name=resInput] li').eq(0).children('input').val()) +
			parseInt(toBlock.find('ul[name=resBar] li').eq(0).children('span.res-value').text()) +
			lastTrade.wood;
	var newStone = parseInt(fromBlock.find('ul[name=resInput] li').eq(1).children('input').val()) +
			parseInt(toBlock.find('ul[name=resBar] li').eq(1).children('span.res-value').text()) +
			lastTrade.stone;
	var newIron = parseInt(fromBlock.find('ul[name=resInput] li').eq(2).children('input').val()) +
			parseInt(toBlock.find('ul[name=resBar] li').eq(2).children('span.res-value').text()) +
			lastTrade.iron;

	toBlock.find('ul[name=resInput] li').eq(0).children('input').val(newWood);
	toBlock.find('ul[name=resInput] li').eq(1).children('input').val(newStone);
	toBlock.find('ul[name=resInput] li').eq(2).children('input').val(newIron);


	if (newWood >= storage) {
		toBlock.find('ul[name=resInput] li').eq(0).children('input').addClass('red');
	} else {
		toBlock.find('ul[name=resInput] li').eq(0).children('input').removeClass('red');
	}
	if (newStone >= storage) {
		toBlock.find('ul[name=resInput] li').eq(1).children('input').addClass('red');
	} else {
		toBlock.find('ul[name=resInput] li').eq(1).children('input').removeClass('red');
	}
	if (newIron >= storage) {
		toBlock.find('ul[name=resInput] li').eq(2).children('input').addClass('red');
	} else {
		toBlock.find('ul[name=resInput] li').eq(2).children('input').removeClass('red');
	}


	var tradeCapacity = fromBlock.find('ul[name=townInfo] li').eq(0).children('input[name=tradeCapacity]').val();
	var totalTrade = parseInt(fromBlock.find('ul[name=resInput] li').eq(0).children('input').val()) +
			parseInt(fromBlock.find('ul[name=resInput] li').eq(1).children('input').val()) +
			parseInt(fromBlock.find('ul[name=resInput] li').eq(2).children('input').val());
	var newCapacity = parseInt(tradeCapacity) - totalTrade;

	fromBlock.find('ul[name=townInfo] li').eq(0).children('span.info-value').html('');
	fromBlock.find('ul[name=townInfo] li').eq(0).children('span.info-value').html(
			tradeCapacity + '<span class="red"> - ' + totalTrade + ' = ' + newCapacity + '</span>'
			);

	if (newCapacity < 0 || parseInt(tradeCapacity) <= 0) {
		butTradeSend.addClass('disabled');
		butTradeSend.attr('disabled', 'disabled');
	} else {
		butTradeSend.removeClass('disabled');
		butTradeSend.removeClass('ui-button-disabled');
		butTradeSend.removeClass('ui-state-disabled');
		butTradeSend.removeAttr('disabled');
	}
}

function getUnitsCost() {
	return {
		sword: {wood: 95, stone: 0, iron: 85, population: 1, favor: 0, type: 'land', characteristic: 'ground'},
		archer: {wood: 120, stone: 0, iron: 75, population: 1, favor: 0, type: 'land', characteristic: 'ground'},
		hoplite: {wood: 0, stone: 75, iron: 150, population: 1, favor: 0, type: 'land', characteristic: 'ground'},
		slinger: {wood: 55, stone: 100, iron: 40, population: 1, favor: 0, type: 'land', characteristic: 'ground'},
		rider: {wood: 240, stone: 120, iron: 360, population: 3, favor: 0, type: 'land', characteristic: 'ground'},
		chariot: {wood: 200, stone: 440, iron: 320, population: 4, favor: 0, type: 'land', characteristic: 'ground'},
		catapult: {wood: 700, stone: 700, iron: 700, population: 15, favor: 0, type: 'land', characteristic: 'ground'},
		attack_ship: {wood: 1300, stone: 300, iron: 800, population: 10, favor: 0, type: 'navy', characteristic: 'sea'},
		bireme: {wood: 800, stone: 700, iron: 180, population: 8, favor: 0, type: 'navy', characteristic: 'sea'},
		trireme: {wood: 2000, stone: 1300, iron: 1300, population: 16, favor: 0, type: 'navy', characteristic: 'sea'},
		small_transporter: {wood: 800, stone: 0, iron: 400, population: 5, favor: 0, type: 'navy', characteristic: 'sea'},
		colonize_ship: {wood: 10000, stone: 10000, iron: 10000, population: 170, favor: 0, type: 'navy', characteristic: 'sea'},
		demolition_ship: {wood: 500, stone: 750, iron: 150, population: 8, favor: 0, type: 'navy', characteristic: 'sea'},
		big_transporter: {wood: 500, stone: 500, iron: 400, population: 7, favor: 0, type: 'navy', characteristic: 'sea'},
		centaur: {wood: 2300, stone: 900, iron: 400, population: 12, favor: 70, type: 'land', characteristic: 'ground'},
		pegasus: {wood: 4000, stone: 1300, iron: 700, population: 20, favor: 120, type: 'land', characteristic: 'sky'},
		griffin: {wood: 4100, stone: 2100, iron: 5200, population: 35, favor: 230, type: 'land', characteristic: 'sky'},
		harpy: {wood: 2000, stone: 500, iron: 1700, population: 14, favor: 85, type: 'land', characteristic: 'sky'},
		fury: {wood: 3300, stone: 6600, iron: 6600, population: 55, favor: 330, type: 'land', characteristic: 'ground'},
		manticore: {wood: 4950, stone: 3375, iron: 3825, population: 45, favor: 270, type: 'land', characteristic: 'sky'},
		godsent: {wood: 0, stone: 0, iron: 0, population: 3, favor: 12, type: 'land', characteristic: 'ground'},
	};
}

function getHardBuildUnits() {
	return {
		'77527': {// Shadow 03
			'sword': 830,
			'archer': 591,
			'hoplite': 328,
			'bireme': 80,
			'small_transporter': 109
		},
		'72400': {// Shadow 02
			'slinger': 1018,
			'rider': 213,
			'hoplite': 237,
			'attack_ship': 44,
			'small_transporter': 119
		},
		'107630': {// Shadow 18
			'slinger': 0,
			'rider': 0,
			'hoplite': 0,
			'catapult': 0,
			'griffin': 0,
			'godsent': 0,
			'attack_ship': 0,
			'small_transporter': 0,
			'colonize_ship': 0
		},
	};
}

function getHeroes() {
	return [
		'andromeda',
		'atalanta',
		'chiron',
		'democritus',
		'ferkyon',
		'helen',
		'heracles',
		'leonidas',
		'orpheus',
		'terylea',
		'urephon',
		'zuretha',
		'jason',
		'odysseus'
	];
}

function setIsland() { // Used for Auto Farm
	var islands = {
		189865: [63830], // Shadow 01
		192218: [72400], // Shadow 02
		192270: [77527, 87053], // Shadow 03, 38
		192239: [73194, 73645], // Shadow 04, 06
		189834: [57859], // Shadow 05
		192364: [101188], // Shadow 07
		192314: [91228, 84194], // Shadow 08, 16
		189817: [107903, 63474], // Shadow 09, 45
		192335: [95605], // Shadow 10
		197115: [107877, 107630], // Shadow 11, 18
		194750: [101551, 121201], // Shadow 12, 21
		190073: [111123, 111072], // Shadow 13, 32
		194798: [110947], // Shadow 14
		190074: [117929], // Shadow 15
		197103: [119779], // Shadow 17
		194746: [102441], // Shadow 19
		197190: [116013], // Shadow 20
		197061: [91839, 92114], // Shadow 22, 25
		192174: [61813], // Shadow 23
		197065: [97209, 112373], // Shadow 24, 30
		197082: [90365], // Shadow 26
		197034: [88220], // Shadow 27
		194754: [98306], // Shadow 28
		197018: [125007], // Shadow 29
		189999: [105800], // Shadow 33
		190053: [113574], // Shadow 34
		194705: [85445], // Shadow 35
		197109: [137303], // Shadow 36
		192275: [82656, 82667], // Shadow 37, 44
		199264: [86963], // Shadow 39
		199360: [158170], // Shadow 40
		199263: [86059, 85978], // Shadow 41, 42
	};

	$.each(townsInfo.towns, function(townId, town) {
		townId = parseInt(townId);
		$.each(islands, function(islandId, towns) {
			if (towns.indexOf(townId) !== -1) {
				townsInfo.towns[townId].island_id = parseInt(islandId);

				return false;
			}
		});
	});
}

function translate(key, translator) {
	if (key in translator) {
		return translator[key];
	}

	return key;
}

function showLoading(openLog) {
	$('.loading div').height($('body').height());

	$('.loading').css('display', '');

	$('.loading img').center();

	var link = document.createElement('link');
	link.type = 'image/x-icon';
	link.rel = 'shortcut icon';
	link.href = 'favicon_loading.ico';
	document.getElementsByTagName('head')[0].appendChild(link);

	if (openLog) {
		// Open Log dialog
		$('#dialogLogs').dialog("open");
		dialogModelClick($('#dialogLogs'));

		$('div[aria-describedby=dialogLogs]').attr('style', 'z-index: 99999 !important');
	}
}
function hideLoading() {
	$('.loading').css('display', 'none');

	var link = document.createElement('link');
	link.type = 'image/x-icon';
	link.rel = 'shortcut icon';
	link.href = 'favicon.ico';
	document.getElementsByTagName('head')[0].appendChild(link);
}

function SortByName(a, b) {
	var aName = a.name.toLowerCase();
	var bName = b.name.toLowerCase();
	return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

function SortByTime(a, b) {
	var aTime = a.time;
	var bTime = b.time;
	return ((aTime < bTime) ? -1 : ((aTime > bTime) ? 1 : 0));
}

function convertTimeString(time) {
	var h = time.split(':')[0];
	var m = time.split(':')[1];
	var s = time.split(':')[2];

	if (typeof h === 'undefined') {
		h = '0';
	}
	if (typeof m === 'undefined') {
		m = '59';
	}
	if (typeof s === 'undefined') {
		s = '59';
	}

	if (h < 10 && h.length < 2) {
		h = '0' + h;
	}
	if (m < 10 && m.length < 2) {
		m = '0' + m;
	}
	if (s < 10 && s.length < 2) {
		s = '0' + s;
	}

	return h + ':' + m + ':' + s;
}

function timestampToString(time) {
	var date = new Date(time);

	return convertTimeString(date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds()) + ' ' +
			date.getUTCDate() + '/' + (date.getUTCMonth() + 1) + '/' + date.getUTCFullYear();
}

function secondsToTime(secs) {
	var hours = Math.floor(secs / (60 * 60));

	var divisor_for_minutes = secs % (60 * 60);
	var minutes = Math.floor(divisor_for_minutes / 60);

	var divisor_for_seconds = divisor_for_minutes % 60;
	var seconds = Math.ceil(divisor_for_seconds);

	var obj = {
		"h": hours,
		"m": minutes,
		"s": seconds
	};
	return obj;
}

function contentHeight() {
	var wHeight = $(window).height();

	$('#tabs').height(wHeight - 9);
	$('#tab-overview').height(wHeight - 239);
	$('#tab-overview').children('.towns-list').height(wHeight - 239);
}

function sendScript(s) {
	socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'script', data: {
			script: s
		}
	});
}

function clearAllTimers() {
	var interval_id = window.setInterval("", 9999);
	for (var i = 1; i < interval_id; i++)
		window.clearInterval(i);
}

function addLog(log, time) {
	var logs = JSON.parse(localStorage.getItem('logs'));

	if (logs === null || typeof logs === 'undefined') {
		logs = {};
	}

	logs[(Object.keys(logs).length + 1)] = {
		time: time,
		text: log
	};

	localStorage.setItem('logs', JSON.stringify(logs));
}

function getLogs() {
	var logs = JSON.parse(localStorage.getItem('logs'));

	if (logs === null || typeof logs === 'undefined') {
		logs = {};
	}

	logs = $.map(logs, function(value, index) {
		return [value];
	});
	logs.sort(SortByTime);

	return logs;
}

function clearTimer(timer) {
	window.clearInterval(timer);
}

function getPastTime(time) {
	var s = Math.floor(Date.now() / 1000) - parseInt(time);
	var m = (s - (s % 60)) / 60;
	s = s % 60;
	var h = (m - (m % 60)) / 60;
	m = m % 60;
	var d = (h - (h % 24)) / 24;
	h = h % 24;

	if (d > 0) {
		return d + 'd ago';
	} else if (h > 0) {
		return h + 'h ago';
	} else if (m > 0) {
		return m + 'm ago';
	} else {
		return s + 's ago';
	}
}

function addLinkToLog() {
	$.each($('#txtLogs table').find('span[data-type=town]'), function() {
		$(this).addClass('click-able');
		$(this).addClass('title');
		$(this).unbind('click');
		$(this).click(function() {
			showLoading(false);

			socket.emit(clientAction, {world: $("select[name=selWorld]").val(), action: 'goto',
				data: {townId: parseInt($(this).attr('data-id'))}});
		});
	});
}

$.fn.center = function() {
	this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
	this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
	return this;
};