var express = require("express");
var app = express();
var port = process.env.PORT || 8000;
var host = process.env.PORT || 'localhost';
var https = require('https');
var fs = require('fs');

if (port === 8000) {
	var key = fs.readFileSync('ssl/grepolis-local-key.pem');
	var cert = fs.readFileSync('ssl/grepolis-local-cert.pem');
} else {
	var key = fs.readFileSync('ssl/grepolis-key.pem');
	var cert = fs.readFileSync('ssl/grepolis-cert.pem');
}

var https_options = {
	key: key,
	cert: cert
};

server = https.createServer(https_options, app).listen(port, host);
console.log('HTTPS Server listening on %s:%s', host, port);

var bodyParser = require('body-parser');
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({// to support URL-encoded bodies
	extended: true
}));

//var io = require('socket.io').listen(app.listen(port)); // HTTP
var io = require('socket.io').listen(server); // HTTPS

app.all('/', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	next();
});

app.get('/', function(req, res, next) {
	res.send('Grepolis Helper Server ...');
});

io.sockets.on('connection', function(socket) {
	socket.emit('ready', {msg: 'Connected to server'});

	// Client send to server
	socket.on('clientToServer', function(data) {
		io.sockets.emit('serverToGrepolis', data);
	});

	// Grepolis return to server
	socket.on('grepolisToServer', function(data) {
		io.sockets.emit('serverToClient', data);
	});
});