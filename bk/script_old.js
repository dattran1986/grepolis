var timmer;
var mood = 100;
var claimType = "normal";
var superMood = false;

function initClaimType () {
	if($('.forced_loyalty').length > 0) {
		superMood = true;
		claimType = "double";
	} else {
		superMood = false;
		claimType = "normal";
	}
}

// Farming
setInterval(function () {
	farm();
}, 360000);

function farm () {
	var farms = $('div[id^="farm_town_"].farmtown_owned').not('[id^="farm_town_flag"]');
	
	if($('.forced_loyalty').length > 0) {
		superMood = true;
	}

	farms.each(function(){
		var farm = $(this);
		var explodeId = farm.attr("id").split("_");
		if(Number(mood) < 80) {
			claimType = "normal";
		}
		
		if(superMood) {
			claimType = "double";
		}
		
		if(explodeId.length >= 3) {
			var id = explodeId[2];
			var URL = "http://en80.grepolis.com/game/farm_town_info?town_id="+Game.townId+"&action=claim_load&h="+Game.csrfToken;
			var json = {"target_id":id,"claim_type":claimType,"time":300,"town_id":Game.townId,"nlreq_id":Game.notification_last_requested_id};
			json = JSON.stringify(json);

			$.ajax({
				url:URL,
				type: "POST",
				data: {json:json},
				success:function(result){
					result = jQuery.parseJSON(result);
					var param = jQuery.parseJSON(result.json.notifications[0].param_str);
					var town = param.Town;
					
					// Display resources
					console.log("Wood: " + town.last_wood + " - Stone: " + town.last_stone + " - Silver: " + town.last_iron);
					
					// Update mood
					mood = result.json.satisfaction;
					console.log("Mood: " + Math.round(mood));
					console.log("Claim type: " + claimType);
			}});
		}
		
		countDown();
	}).delay(2000);
}

function countDown () {
	var i = 360;
	
	clearTimeout(timmer);
	
	timmer = setInterval(function () {
		$(".town_name").text(Game.townName + " ("+i+")");
		i--;
	}, 1000);
}

function checkStatus() {
	var checker = setInterval(function () {
		var farms = $('div[id^="farm_town_"].farmtown_owned').not('[id^="farm_town_flag"]');
		
		farms.each(function(){
			clear();
			var farm = $(this);
			var resBar = farm.children('span');
			var resAvailable = resBar.children('.res_available');
			if(!resAvailable.hasClass('not')){
				console.log("Ready to be claimed");
				
				if($('.forced_loyalty').length > 0) {
					console.log("Claim type = double");
				}
			}
		});
	}, 300 * 1000); // 5 minutes
}

farm();








function getTowns(){
	var url = "http://en80.grepolis.com/game/data?town_id=72400&action=get&h=" + Game.csrfToken;
	var json = {"types":[{"type":"map","param":{"x":10,"y":7}},{"type":"bar"},{"type":"backbone"}],"town_id":63830,"nlreq_id":0};
	json = JSON.stringify(json);
	
	$.ajax({
		url:url,
		type: "POST",
		data: {json:json},
		success:function(result){
			console.log(result);
	}});
}

function getBuildings () {
	var url = "http://en80.grepolis.com/game/frontend_bridge?town_id=63830&action=execute&h=" + Game.csrfToken;
	var json = {"model_url":"BuildingBuildData/63830","action_name":"forceUpdate","arguments":null,"town_id":63830,"nlreq_id":Game.notification_last_requested_id};
	json = JSON.stringify(json);
	
	$.ajax({
		url:url,
		type: "POST",
		data: {json:json},
		success:function(result){
			console.log(result);
	}});
}